﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EvaluationFunction
{
    // Do the logic to evaluate the state of the game !
    public float evaluate(State s)
    {

        float pontuacao = s.Score;
        //Nº Peças em jogo
        int nPecas = s.PlayersUnits.Count;
        int nPecasAdversario = s.AdversaryUnits.Count;

        pontuacao += nPecas;
        pontuacao -= nPecasAdversario;
        //Hp Total 
        float hpTotalPlayer = 0;
        float hpTotalAdversary = 0;

        //ATK Total
        float atkTotalPlayer = 0;
        float atkTotalAdversary = 0;


        //Calculo Hp e ATK Total
        foreach (Unit i in s.PlayersUnits)
        {
            hpTotalPlayer += i.hp + i.GetBonus(s.board, s.PlayersUnits).Item1;

            atkTotalPlayer += i.attack + i.GetBonus(s.board, s.PlayersUnits).Item2;
        }

        foreach (Unit i in s.AdversaryUnits)
        {
            hpTotalAdversary += i.hp + i.GetBonus(s.board, s.AdversaryUnits).Item1;

            atkTotalAdversary += i.attack + i.GetBonus(s.board, s.AdversaryUnits).Item2;
        }

        //Quanto maior hp total adversário menor a pontação

        pontuacao -= hpTotalAdversary;

        //Quanto maior hp total das minhas unidades maior a pontação

        pontuacao +=  hpTotalPlayer;

        //Garente que ataca a unidade de maior ataque
        if (s.isAttack)
        {
            if (s.PlayersUnits.Contains(s.unitAttacked))
            {
                pontuacao -= s.unitAttacked.attack;
            }
            else
            {
                pontuacao += s.unitAttacked.attack;
              
            }
  
        }
       
        //Quanto maior o ataque total do adversário menor a pontuação
        pontuacao -= atkTotalAdversary;

        //Mais ataque = mais buffs = mais pontuação
        pontuacao += atkTotalPlayer;
        
        //Quantas mais peças me podem atacar menor a pontuação
        foreach (Unit i in s.AdversaryUnits)
        {
            foreach (Unit j in i.GetAttackable(s, s.PlayersUnits))
            {
                foreach (Unit h in s.PlayersUnits)
                {
                    if (j.Equals(h))
                    {
                        pontuacao += -10;
                    }

                }
            }
        }
    
        return pontuacao;
    }
}

