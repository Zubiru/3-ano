﻿using UnityEngine;
using System.Collections;
using System;


public class UtilityFunction
{

    public float evaluate(State s)
    {
        if (s.PlayersUnits.Count > 0 && s.AdversaryUnits.Count == 0)
        {
            return 100000-s.depth;
        }
        else if (s.PlayersUnits.Count == 0 && s.AdversaryUnits.Count > 0)
        {
            return s.depth-100000;
        }
        else
        {
            return 0;
        }
    }
}
