﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DeepCopyExtensions;
using System.Linq;

public class MinMaxSemCorte : MoveMaker
{
    public EvaluationFunction evaluator;
    private UtilityFunction utilityfunc;
    public int depth = 0;
    private PlayerController MaxPlayer;
    private PlayerController MinPlayer;
    public int numNodes = 0;

    public MinMaxSemCorte(PlayerController MaxPlayer, EvaluationFunction eval, UtilityFunction utilf, PlayerController MinPlayer)
    {
        this.MaxPlayer = MaxPlayer;
        this.MinPlayer = MinPlayer;
        this.evaluator = eval;
        this.utilityfunc = utilf;
    }

    public override State MakeMove()
    { 
        // The move is decided by the selected state
        return GenerateNewState();
    }

    private State GenerateNewState()
    {
        // Creates initial state
        State newState = new State(this.MaxPlayer, this.MinPlayer);
        // Call the MinMax implementation
        State bestMove = MinMax(newState, true, float.MinValue, float.MaxValue, newState);
        Debug.Log(" score" + bestMove.Score);
        // returning the actual state. You should modify this
        numNodes += MaxPlayer.ExpandedNodes;
        Debug.Log("Nos Total: " + numNodes);
        MaxPlayer.ExpandedNodes = 0;
        return bestMove;
    }

    public State MinMax(State s, bool isMax, float alpha, float beta, State estadoInicial)
    {
        if (s.depth != 0)
        {
            float aux = utilityfunc.evaluate(s);
            if (utilityfunc.evaluate(s) != 0)
            {
                s.Score = aux;
                return s;
            }
            // Aqui altera-se a profundidade
            if (s.depth > 4 || MaxPlayer.ExpandedNodes >= MaxPlayer.MaximumNodesToExpand)
            {
                return s;
            }
        }
        float bestScore;
        State aux2 = null;
        if (isMax)
        {
            List<State> possibilidades = GeneratePossibleStates(s, true);
            bestScore = float.MinValue;
            foreach (State p in possibilidades)
            {
                State aux = MinMax(p, false, alpha, beta, estadoInicial);
                if (aux.Score > bestScore)
                {
                    bestScore = aux.Score;
                    aux2 = p;
                    aux2.Score = bestScore;
                }
            }
            if (s.depth == 0)
            {
                return aux2;
            }
        }
        else
        {
            List<State> possibilidades = GeneratePossibleStates(new State(s), false);
            bestScore = float.MaxValue;
            foreach (State p in possibilidades)
            {
                State aux = MinMax(new State(p), true, alpha, beta, estadoInicial);
                if (aux.Score < bestScore)
                {
                    bestScore = aux.Score;
                }
            }
        }
        s.Score = bestScore;
        return s;
    }




    private List<State> GeneratePossibleStates(State state, Boolean max)
    {
        List<State> states = new List<State>();
        //Generate the possible states available to expand
        foreach (Unit currentUnit in state.PlayersUnits)
        {
            // Movement States
            List<Tile> neighbours = currentUnit.GetFreeNeighbours(state);
            foreach (Tile t in neighbours)
            {
                State newState = new State(state, currentUnit, true);
                newState = MoveUnit(newState, t);
                states.Add(newState);
            }
            // Attack states
            List<Unit> attackOptions = currentUnit.GetAttackable(state, state.AdversaryUnits);
            foreach (Unit t in attackOptions)
            {
                State newState = new State(state, currentUnit, false);
                newState = AttackUnit(newState, t);
                states.Add(newState);
            }

        }


        // YOU SHOULD NOT REMOVE THIS
        // Counts the number of expanded nodes;
        this.MaxPlayer.ExpandedNodes += states.Count;
        //

        foreach (State s in states)
        {
            s.Score = state.Score;

            //Se min altera perspetiva para avaliar
            if (!max)
            {
                float aux = utilityfunc.evaluate(new State(s));
                if (aux != 0)
                {
                    s.Score = aux;

                }
                else
                {
                    s.Score = evaluator.evaluate(new State(s));
                }

            }
            else
            {
                float aux = utilityfunc.evaluate(s);
                if (aux != 0)
                {
                    s.Score = aux;

                }
                else
                {
                    s.Score = evaluator.evaluate(s);
                }
            }

        }
        //Ordenação dos estados para maximizar o corte
        if (max)
            states = states.OrderByDescending(x => x.Score).ToList();
        else
            states = states.OrderBy(x => x.Score).ToList();

        return states;
    }

    private State MoveUnit(State state, Tile destination)
    {
        Unit currentUnit = state.unitToPermormAction;
        //First: Update Board
        state.board[(int)destination.gridPosition.x, (int)destination.gridPosition.y] = currentUnit;
        state.board[currentUnit.x, currentUnit.y] = null;
        //Second: Update Players Unit Position
        currentUnit.x = (int)destination.gridPosition.x;
        currentUnit.y = (int)destination.gridPosition.y;
        state.isMove = true;
        state.isAttack = false;
        return state;
    }
    //Alterada a função de forma a atualizar a hp das unidades nas listas dos jogadores
    private State AttackUnit(State state, Unit toAttack)
    {
        Unit currentUnit = state.unitToPermormAction;
        Unit attacked = toAttack.DeepCopyByExpressionTree();
        List<Unit> PlayersUnits = state.PlayersUnits.DeepCopyByExpressionTree();
        List<Unit> AdversaryUnits = state.AdversaryUnits.DeepCopyByExpressionTree();

        Tuple<float, float> currentUnitBonus = currentUnit.GetBonus(state.board, state.PlayersUnits);
        Tuple<float, float> attackedUnitBonus = attacked.GetBonus(state.board, state.AdversaryUnits);


        attacked.hp += Math.Min(0, (attackedUnitBonus.Item1)) - (currentUnitBonus.Item2 + currentUnit.attack);
        state.unitAttacked = attacked;
        state.PlayersUnits = PlayersUnits;
        state.AdversaryUnits = AdversaryUnits;
        foreach (Unit i in AdversaryUnits)
        {
            if (i != null)
            {
                if (i.Equals(attacked))
                {
                    i.hp = attacked.hp;
                }
            }

        }
        foreach (Unit i in PlayersUnits)
        {
            if (i != null)
            {
                if (i.Equals(attacked))
                {
                    i.hp = attacked.hp;
                }
            }

        }


        if (attacked.hp <= 0)
        {
            //Board update by killing the unit!
            state.board[attacked.x, attacked.y] = null;
            int index = state.AdversaryUnits.IndexOf(attacked);
            state.AdversaryUnits.RemoveAt(index);

        }
        state.isMove = false;
        state.isAttack = true;

        return state;

    }
}
