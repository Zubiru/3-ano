Projetos 3ºAno de Licenciatura.

1. ProjetoBD, base de dados Postgres, client e servidor em Java.
2. ProjetoCG, projeto de computação gráfica em c++, iluminação, texturas,etc.
3. ProjetoCompiladores, compilador para subset de GO, uso de regex e programação em C.
4. ProjetoIIA, implementação do algoritmo minmax e função de avaliação em C#.
5. ProjetoIIA-2, implementação do algoritmo genético e função de fitness em C#.
6. ProjetoSD, implementação de um modelo servidor/client RMI e com servidor de backup + interface web simples + REST. Linguagem Java.
