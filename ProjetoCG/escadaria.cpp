#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <glut.h>
#include <iostream>
#include <time.h>
#include "materiais.h"
#include "RgbImage.h"

//--------------------------------- Definir cores
#define BLUE     0.0, 0.0, 1.0, 1.0
#define RED		 1.0, 0.0, 0.0, 1.0
#define YELLOW	 1.0, 1.0, 0.0, 1.0
#define GREEN    0.0, 1.0, 0.0, 1.0
#define WHITE    1.0, 1.0, 1.0, 1.0
#define BLACK    0.0, 0.0, 0.0, 1.0
#define PI		 3.14159

#define frand()			((float)rand()/RAND_MAX)
#define MAX_PARTICULAS  600

int randomSeed=12;


//---------------------------------------- Particle attributes
typedef struct {
	float   size;		// tamanho
	float	life;		// vida
	float	fade;		// fade
	float	r, g, b;    // color
	GLfloat x, y, z;    // posicao
	GLfloat vx, vy, vz; // velocidade 
    GLfloat ax, ay, az; // aceleracao
} Particle;

Particle  particula1[MAX_PARTICULAS*2];
GLint    milisec = 1000; 

GLint		wScreen=800, hScreen=600;	
GLdouble		xTam=20.0, yTam=15.0, zTam=20.0;
GLint 		largTable=2, compTable=10, altTable=3;

GLdouble  ang_observador =0.5*PI;
GLdouble  raio_observador = 1.0;
GLdouble  inclinacao_observador = 0.0;
GLdouble  posicao_observador[] ={0, 4, 0};
GLdouble  olhar_observador[]={posicao_observador[0]+raio_observador*cos(ang_observador),posicao_observador[1],posicao_observador[2]-raio_observador*sin(ang_observador)};


GLfloat  angZoom=109.0;
GLfloat  incZoom=3;
GLfloat  xObj=-17.0, zObj=-8.0;
GLfloat  angR = 0;

//META 2
GLuint   texturas[10];
RgbImage image;
GLfloat  repeat = 1.0;
float luzGlobal[] = {0.2,0.2,0.2,1.0};

int ligaPontual=1;
int ligaFoco=1;
int ligaTv=1;
int canal=1;

GLfloat rFoco=1.1, aFoco=ang_observador;
GLfloat incH =0.0, incV=0.0;
GLfloat focoExp   = 10;
GLfloat focoCut   = 30;
GLfloat posFoco [] = {0.0,4.0,0.0,1.0};
GLfloat dirFoco [] = {olhar_observador[0]-posicao_observador[0],0,olhar_observador[2]-posicao_observador[2]};


GLfloat focoExpTv   = 0;
GLfloat focoCutTv   = 90;
GLfloat posFocoTv [] = {4.5,altTable+3,17.5,1.0};
GLfloat dirFocoTv [] = {0,0,-1};

GLfloat focoExpToTv   = 0;
GLfloat focoCutToTv   = 65;
GLfloat posFocoToTv [] = {4.5,altTable+3.5,15,1.0};
GLfloat dirFocoToTv [] = {0,0,1};


void iniParticulas(Particle *particula)
{

 srand(randomSeed);
 GLfloat v, theta, phi;
 int i;
 GLfloat px, py, pz;
 GLfloat ps;

	px = rand() % (compTable-1) +1;
	py = rand() % (altTable*3-(altTable+1)) +altTable+1;
	pz = 0.0;
	ps = 0.08 ;



 for(i=0; i<MAX_PARTICULAS; i++)  {

	//---------------------------------  
	v     = frand()*0.025;
    theta = 2.0*frand()*M_PI;  
	phi   = frand()*M_PI;	
    particula[i].size = ps ;		
	particula[i].x	  = px + 0.1*frand();    
	particula[i].y	  = py + 0.1*frand();   
    particula[i].z	  = 0.0;
        
	particula[i].vx = v * cos(theta) * sin(phi);
    particula[i].vy = v * cos(phi);

	particula[i].r = frand();
	particula[i].g = frand();	
	particula[i].b = frand();	
	particula[i].life = 1.5f;		                
	particula[i].fade = 0.01f;	
  }
  randomSeed=randomSeed*4/2+2;
  srand(time(NULL));
  
  
	px = rand() % (compTable-1) +1;
	py = rand() % (altTable*3-(altTable+1)) +altTable+1;
	pz = 0.0;
	ps = 0.08 ;



 for(i=MAX_PARTICULAS; i<MAX_PARTICULAS*2; i++)  {

	//---------------------------------  
	v     = frand()*0.025;
    theta = 2.0*frand()*M_PI;   
	phi   = frand()*M_PI;	
    particula[i].size = ps ;	
	particula[i].x	  = px + 0.1*frand();    
	particula[i].y	  = py + 0.1*frand();    
    particula[i].z	  = 0.0;
        
	particula[i].vx = v * cos(theta) * sin(phi);
    particula[i].vy = v * cos(phi);

	particula[i].r = frand();
	particula[i].g = frand();	
	particula[i].b = frand();	
	particula[i].life = 1.5f;		                
	particula[i].fade = 0.01f;	
  }
  
  
  
  
  
}


void showParticulas(Particle *particula, int inicio, int fim) {
 int i;
 for (i=inicio; i<fim; i++){
	if(particula[i].life>0.0 && particula[i].x>1 &&  particula[i].x<compTable-1 && particula[i].y > altTable+1 && particula[i].y < altTable*3){
		glEnable(GL_COLOR_MATERIAL);
		glColor3f(particula[i].r,particula[i].g,particula[i].b);
	 	glBegin(GL_POINTS);				        
			/*glTexCoord2d(0,0);*/ glVertex3f(particula[i].x, particula[i].y, particula[i].z);      
			/*glTexCoord2d(1,0);*/ //glVertex3f(particula[i].x +particula[i].size, particula[i].y -particula[i].size, particula[i].z);        
			/*glTexCoord2d(1,1);*/ //glVertex3f(particula[i].x +particula[i].size, particula[i].y +particula[i].size, particula[i].z);            
			/*glTexCoord2d(0,1)*/ //glVertex3f(particula[i].x -particula[i].size, particula[i].y +particula[i].size, particula[i].z);       
		glEnd();	
		particula[i].x += particula[i].vx;
	    particula[i].y += particula[i].vy;
	    particula[i].z += particula[i].vz;
	    particula[i].vx += particula[i].ax;
	    particula[i].vy += particula[i].ay;
	    particula[i].vz += 0;
		particula[i].life -= particula[i].fade;	
		glDisable(GL_COLOR_MATERIAL);	
	}
}

}

void initMaterials(int material) {
	
	switch (material){
	case 0: //esmerald
		glMaterialfv(GL_FRONT,GL_AMBIENT,  esmeraldAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  esmeraldDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, esmeraldSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,esmeraldCoef);
		break;
	case 1: //jade
		glMaterialfv(GL_FRONT,GL_AMBIENT,  jadeAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  jadeDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, jadeSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,jadeCoef);
		break;
	case 2: //obsidian
		glMaterialfv(GL_FRONT,GL_AMBIENT,  obsidianAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  obsidianDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, obsidianSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,obsidianCoef);
		break;
	case 3: //pearl
		glMaterialfv(GL_FRONT,GL_AMBIENT,  pearlAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  pearlDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, pearlSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,pearlCoef);
		break;
	case 4: //ruby
		glMaterialfv(GL_FRONT,GL_AMBIENT,  rubyAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  rubyDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, rubySpec);
		glMaterialf (GL_FRONT,GL_SHININESS,rubyCoef);
		break;
	case 5: //turquoise
		glMaterialfv(GL_FRONT,GL_AMBIENT,  turquoiseAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  turquoiseDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, turquoiseSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,turquoiseCoef);
		break;
	case 6: //brass
		glMaterialfv(GL_FRONT,GL_AMBIENT,  brassAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  brassDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, brassSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,brassCoef);
		break;
	case 7: //bronze
		glMaterialfv(GL_FRONT,GL_AMBIENT,  bronzeAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  bronzeDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, bronzeSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,bronzeCoef);
		break;
	case 8: //chrome
		glMaterialfv(GL_FRONT,GL_AMBIENT,  chromeAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  chromeDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, chromeSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,chromeCoef);
		break;
	case 9: //copper
		glMaterialfv(GL_FRONT,GL_AMBIENT,  copperAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  copperDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, copperSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,copperCoef);
		break;
	case 10: //gold
		glMaterialfv(GL_FRONT,GL_AMBIENT,  goldAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  goldDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, goldSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,goldCoef);
		break;
	case 11: //silver
		glMaterialfv(GL_FRONT,GL_AMBIENT,  silverAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  silverDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, silverSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,silverCoef);
		break;
	case 12: //blackPlastic
		glMaterialfv(GL_FRONT,GL_AMBIENT,  blackPlasticAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  blackPlasticDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, blackPlasticSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,blackPlasticCoef);
		break;
	case 13: //cyankPlastic
		glMaterialfv(GL_FRONT,GL_AMBIENT,  cyanPlasticAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  cyanPlasticDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, cyanPlasticSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,cyanPlasticCoef);
		break;
	case 14: //greenPlastic
		glMaterialfv(GL_FRONT,GL_AMBIENT,  greenPlasticAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  greenPlasticDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, greenPlasticSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,greenPlasticCoef);
		break;
	case 15: //redPlastic
		glMaterialfv(GL_FRONT,GL_AMBIENT,  redPlasticAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  redPlasticDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, redPlasticSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,redPlasticCoef);
		break;
	case 16: //whitePlastic
		glMaterialfv(GL_FRONT,GL_AMBIENT,  whitePlasticAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  whitePlasticDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, whitePlasticSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,whitePlasticCoef);
		break;
	case 17: //yellowPlastic
		glMaterialfv(GL_FRONT,GL_AMBIENT,  yellowPlasticAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  yellowPlasticDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, yellowPlasticSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,yellowPlasticCoef);
		break;
	case 18: //blackRubber
		glMaterialfv(GL_FRONT,GL_AMBIENT,  blackRubberAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  blackRubberDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, blackRubberSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,blackRubberCoef);
		break;
	case 19: //cyanRubber
		glMaterialfv(GL_FRONT,GL_AMBIENT,  cyanRubberAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  cyanRubberDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, cyanRubberSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,cyanRubberCoef);
		break;
	case 20: //greenRubber
		glMaterialfv(GL_FRONT,GL_AMBIENT,  greenRubberAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  greenRubberDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, greenRubberSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,greenRubberCoef);
		break;
	case 21: //redRubber
		glMaterialfv(GL_FRONT,GL_AMBIENT,  redRubberAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  redRubberDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, redRubberSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,redRubberCoef);
		break;
	case 22: //redRubber
		glMaterialfv(GL_FRONT,GL_AMBIENT,  whiteRubberAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  whiteRubberDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, whiteRubberSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,whiteRubberCoef);
		break;
	case 23: //redRubber
		glMaterialfv(GL_FRONT,GL_AMBIENT,  yellowRubberAmb  );
		glMaterialfv(GL_FRONT,GL_DIFFUSE,  yellowRubberDif );
		glMaterialfv(GL_FRONT,GL_SPECULAR, yellowRubberSpec);
		glMaterialf (GL_FRONT,GL_SHININESS,yellowRubberCoef);
		break;
	}		
}



void initTexturas(){
	glGenTextures(1, &texturas[0]);
	glBindTexture(GL_TEXTURE_2D, texturas[0]);
	image.LoadBmpFile("chao.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData()); 
		
		
		
	glGenTextures(1, &texturas[1]);
	glBindTexture(GL_TEXTURE_2D, texturas[1]);
	image.LoadBmpFile("janela.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData()); 
	
	glGenTextures(1, &texturas[3]);
	glBindTexture(GL_TEXTURE_2D, texturas[3]);
	image.LoadBmpFile("parede.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData()); 
		
		
	glGenTextures(1, &texturas[4]);
	glBindTexture(GL_TEXTURE_2D, texturas[4]);
	image.LoadBmpFile("madeira.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_CLAMP);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData());
		
	glGenTextures(1, &texturas[5]);
	glBindTexture(GL_TEXTURE_2D, texturas[5]);
	image.LoadBmpFile("tv.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData()); 
		
	glGenTextures(1, &texturas[6]);
	glBindTexture(GL_TEXTURE_2D, texturas[6]);
	image.LoadBmpFile("tvapagada.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData()); 
		
	glGenTextures(1, &texturas[7]);
	glBindTexture(GL_TEXTURE_2D, texturas[7]);
	image.LoadBmpFile("tvapagada.bmp");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);  
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 
		image.GetNumCols(),
		image.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
		image.ImageData());
}

void initLights(){
	glEnable(GL_LIGHTING);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,luzGlobal); 
	float posPontual [] = {0.0,15.0,0.0,1.0};
	float luzAmbiente [] = {1.0,1.0,1.0,1.0};
	float luzDifusa [] = {1.0,1.0,1.0,1.0};
	float luzEspecular [] = {1.0,1.0,1.0,1.0};

	glLightfv(GL_LIGHT0,GL_POSITION, posPontual);
	glLightfv(GL_LIGHT0,GL_AMBIENT, luzAmbiente);
	glLightfv(GL_LIGHT0,GL_DIFFUSE, luzDifusa);
	glLightfv(GL_LIGHT0,GL_SPECULAR, luzEspecular);
	
	
	glLightfv(GL_LIGHT1,GL_POSITION, posFoco);
	glLightfv(GL_LIGHT1,GL_SPOT_DIRECTION, dirFoco);
	glLightf(GL_LIGHT1,GL_SPOT_EXPONENT,focoExp);
	glLightf(GL_LIGHT1,GL_SPOT_CUTOFF,focoCut);
	glLightfv(GL_LIGHT1,GL_DIFFUSE, luzDifusa);
	glLightfv(GL_LIGHT1,GL_SPECULAR, luzEspecular);

	float luzDifusaTv [] = {0.8,0.6,0.0,0.3}; 
	float luzEspecularTv [] = {0.8,0.6,0.0,0.3};
	
	
	glLightfv(GL_LIGHT2,GL_POSITION, posFocoTv);
	glLightfv(GL_LIGHT2,GL_SPOT_DIRECTION, dirFocoTv);
	glLightf(GL_LIGHT2,GL_SPOT_EXPONENT,focoExpTv);
	glLightf(GL_LIGHT2,GL_SPOT_CUTOFF,focoCutTv);
	glLightfv(GL_LIGHT2,GL_DIFFUSE, luzDifusaTv);
	glLightfv(GL_LIGHT2,GL_SPECULAR, luzEspecularTv);
	
	float luzDifusaToTv [] = {1,1,1,1.0};
	float luzEspecularToTv [] = {1,1,1,1.0};
	
	
	glLightfv(GL_LIGHT3,GL_POSITION, posFocoToTv);
	glLightfv(GL_LIGHT3,GL_SPOT_DIRECTION, dirFocoToTv);
	glLightf(GL_LIGHT3,GL_SPOT_EXPONENT,focoExpToTv);
	glLightf(GL_LIGHT3,GL_SPOT_CUTOFF,focoCutToTv);
	glLightfv(GL_LIGHT3,GL_DIFFUSE, luzDifusaToTv);
	glLightfv(GL_LIGHT3,GL_SPECULAR, luzEspecularToTv);
	
	if(ligaTv>0){
		glEnable(GL_LIGHT2);
		glEnable(GL_LIGHT3);
	}
	else{
		glDisable(GL_LIGHT2);
		glDisable(GL_LIGHT3);
	}
		
	if(ligaPontual>0)
		glEnable(GL_LIGHT0);
	else
		glDisable(GL_LIGHT0);
	
	if(ligaFoco>0)
		glEnable(GL_LIGHT1);
	else
		glDisable(GL_LIGHT1);
	
}

void inicializa(void)
{
	glClearColor(BLACK);		
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
	initTexturas();
	//iniParticulas(particula1);
}

void drawEixos(){	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Eixo X
	glColor4f(RED);
	glBegin(GL_LINES);
		glVertex3i( 0, 0, 0); 
		glVertex3i(10, 0, 0); 
	glEnd();
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Eixo Y
	glColor4f(GREEN);
	glBegin(GL_LINES);
		glVertex3i(0,  0, 0); 
		glVertex3i(0, 10, 0); 
	glEnd();
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Eixo Z
	glColor4f(BLUE);
	glBegin(GL_LINES);
		glVertex3i( 0, 0, 0); 
		glVertex3i( 0, 0,10); 
	glEnd();

}	
void drawTable(){
	//Table
	glPushMatrix();
	glTranslatef(0,0,19);
	initMaterials(2);
	//Base mesa
	glBegin(GL_QUADS);
		glVertex3f(0,0.1,0);
		glVertex3f(0,0.1,-largTable);
		glVertex3f(compTable,0.1,-largTable);
		glVertex3f(compTable,0.1,0);
	glEnd();
		
	glBegin(GL_QUADS);
		glVertex3f(0,altTable,0);
		glVertex3f(0,altTable,-largTable);
		glVertex3f(compTable,altTable,-largTable);
		glVertex3f(compTable,altTable,0);
	glEnd();
	
	glBegin(GL_QUADS);
		glVertex3f(0,altTable/2,0);
		glVertex3f(0,altTable/2,-largTable);
		glVertex3f(compTable,altTable/2,-largTable);
		glVertex3f(compTable,altTable/2,0);
	glEnd();
	
	//Lateral Mesa
	glBegin(GL_QUADS);
		glVertex3f(0,0.1,0);
		glVertex3f(0,0.1,-largTable);
		glVertex3f(0,altTable,-largTable);
		glVertex3f(0,altTable,0);
	glEnd();
	
	glBegin(GL_QUADS);
		glVertex3f(compTable,0.1,0);
		glVertex3f(compTable,0.1,-largTable);
		glVertex3f(compTable,altTable,-largTable);
		glVertex3f(compTable,altTable,0);
	glEnd();
	glPopMatrix();

}
void drawTv(){
	glPushMatrix();
	glTranslatef(0,0,17.5);
	initMaterials(16);
	glEnable(GL_TEXTURE_2D);
	if(ligaTv>=1){
		if(canal==1)
			glBindTexture(GL_TEXTURE_2D,texturas[5]);
		else if(canal==2)
			glBindTexture(GL_TEXTURE_2D,texturas[6]);	
	}else
		glBindTexture(GL_TEXTURE_2D,texturas[6]);
	glBegin(GL_QUADS);
		glTexCoord2f(1.0f,0.0f); glVertex3f(1,altTable+1,0);
		glTexCoord2f(1.0f,1.0f); glVertex3f(1,altTable*3,0);
		glTexCoord2f(0.0f,1.0f); glVertex3f(compTable-1,altTable*3,0);
		glTexCoord2f(0.0f,0.0f); glVertex3f(compTable-1,altTable+1,0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	if(ligaTv>=1 && canal==2){
		showParticulas(particula1,0,MAX_PARTICULAS);
		showParticulas(particula1,MAX_PARTICULAS,MAX_PARTICULAS*2);
	
	}
	
	
	//Base 
	initMaterials(12);
	GLfloat nBase [] = {0.0,0.0,-1.0}; 
	glNormal3fv(nBase);
	glBegin(GL_QUADS);
		glVertex3f(4.75,altTable,0);
		glVertex3f(4.75,altTable+1,0);
		glVertex3f(5.25,altTable+1,0);
		glVertex3f(5.25,altTable,0);
	glEnd();	
	
	initMaterials(12);
	GLfloat nBase2 [] = {0.0,1.0,0.0}; 
	glNormal3fv(nBase2);
	glBegin(GL_QUADS);
		glVertex3f(4,altTable+0.1,-0.25);
		glVertex3f(4,altTable+0.1,0.25);
		glVertex3f(6,altTable+0.1,0.25);
		glVertex3f(6,altTable+0.1,-0.25);
	glEnd();	
	glPopMatrix();

}

void drawChaleira() {	
		//glBindTexture(GL_TEXTURE_2D,texture[2]);		
		glFrontFace(GL_CW);					// face frente cntrαrio normal
		
		initMaterials(4);		
		glPushMatrix();
			glTranslatef (4.5,1.2,8);		
			glutSolidTeapot(1.2);
		glPopMatrix();
		glFrontFace(GL_CCW);
}
void drawSala(){
	//chao
	initMaterials(16);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texturas[0]);
	GLfloat n0 [] = {0.0,1.0,0.0}; 
	glNormal3fv(n0);
	glBegin(GL_QUADS);
		glTexCoord2f(repeat,0.0f); glVertex3f(xTam,0,zTam);       
		glTexCoord2f(repeat,repeat); glVertex3f(xTam,0,-zTam);   
		glTexCoord2f(0.0f,repeat); glVertex3f(-xTam,0,-zTam);  
		glTexCoord2f(0.0f,0.0f); glVertex3f(-xTam,0,zTam);   
	glEnd();
	glDisable(GL_TEXTURE_2D);
	//teto
	GLfloat n1 [] = {0.0,-1.0,0.0}; 
	glNormal3fv(n1);
	initMaterials(0);
	glBegin(GL_QUADS);
		glVertex3f(xTam,yTam,zTam);
		glVertex3f(xTam,yTam,-zTam);
		glVertex3f(-xTam,yTam,-zTam);
		glVertex3f(-xTam,yTam,zTam);
	glEnd();
	//face direita
	GLfloat n2 [] = {-1.0,0.0,0.0}; 
	glNormal3fv(n2);
	//initMaterials(1);
	initMaterials(16);	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texturas[3]);
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f,0.0f); glVertex3f(xTam,0,zTam);
		glTexCoord2f(1.0f,0.0f); glVertex3f(xTam,0,-zTam);
        glTexCoord2f(1.0f,1.0f); glVertex3f(xTam,yTam,-zTam);
	    glTexCoord2f(0.0f,1.0f); glVertex3f(xTam,yTam,zTam);
	glEnd();
	
	//Janela
	initMaterials(16);
	GLfloat nJanela [] = {-1.0,0.0,0.0}; 
	glNormal3fv(nJanela);
//	if(luzGlobal[0]>0){//Janela aberta
		glBindTexture(GL_TEXTURE_2D,texturas[1]);
		glBegin(GL_QUADS);
			glTexCoord2f(0.0f,0.0f); glVertex3f(xTam-0.1,yTam/6,zTam/6);
			glTexCoord2f(1.0f,0.0f); glVertex3f(xTam-0.1,yTam/6,-zTam/6);
	        glTexCoord2f(1.0f,1.0f); glVertex3f(xTam-0.1,yTam/1.5,-zTam/6);
		    glTexCoord2f(0.0f,1.0f); glVertex3f(xTam-0.1,yTam/1.5,zTam/6);
		glEnd();
//	}	
	//Janela fechada
	
	
	glDisable(GL_TEXTURE_2D);
	//face esquerda
	GLfloat n3 [] = {1.0,0.0,0.0}; 
	glNormal3fv(n3);
	initMaterials(2);
	glBegin(GL_QUADS);
		glVertex3f(-xTam,0,zTam);
		glVertex3f(-xTam,0,-zTam);
		glVertex3f(-xTam,yTam,-zTam);
		glVertex3f(-xTam,yTam,zTam);
	glEnd();
	//face frente
	GLfloat n4 [] = {0.0,0.0,1.0}; 
	glNormal3fv(n4);
	glColor4f(GREEN);
	initMaterials(10);
	glBegin(GL_QUADS);
		glVertex3f(xTam,0,-zTam);
		glVertex3f(-xTam,0,-zTam);
		glVertex3f(-xTam,yTam,-zTam);
		glVertex3f(xTam,yTam,-zTam);
	glEnd();
	//face tras
	initMaterials(4);
	glColor4f(BLUE);
	GLfloat n5 [] = {0.0,0.0,-1.0}; 
	glNormal3fv(n5);
	glBegin(GL_QUADS);
		glVertex3f(xTam,0,zTam);
		glVertex3f(-xTam,0,zTam);
		glVertex3f(-xTam,yTam,zTam);
		glVertex3f(xTam,yTam,zTam);
	glEnd();
	glColor4f(BLACK);
	//direita
	glBegin(GL_LINES);
		glVertex3f(xTam,0,zTam);
		glVertex3f(xTam,0,-zTam);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(xTam,yTam,zTam);
		glVertex3f(xTam,yTam,-zTam);
	glEnd();

	//esquerda
	glBegin(GL_LINES);
		glVertex3f(-xTam,0,zTam);
		glVertex3f(-xTam,0,-zTam);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(-xTam,yTam,zTam);
		glVertex3f(-xTam,yTam,-zTam);
	glEnd();
	//frente
	glBegin(GL_LINES);
		glVertex3f(xTam,0,-zTam);
		glVertex3f(-xTam,0,-zTam);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(xTam,yTam,-zTam);
		glVertex3f(-xTam,yTam,-zTam);
	glEnd();
	//tras
	glBegin(GL_LINES);
		glVertex3f(xTam,0,zTam);
		glVertex3f(-xTam,0,zTam);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(xTam,yTam,zTam);
		glVertex3f(-xTam,yTam,zTam);
	glEnd();
	//1
	glBegin(GL_LINES);
		glVertex3f(xTam,0,zTam);
		glVertex3f(xTam,yTam,zTam);
	glEnd();
	//2
	glBegin(GL_LINES);
		glVertex3f(xTam,0,-zTam);
		glVertex3f(xTam,yTam,-zTam);
	glEnd();
	//3
	glBegin(GL_LINES);
		glVertex3f(-xTam,0,-zTam);
		glVertex3f(-xTam,yTam,-zTam);
	glEnd();
	//4
	glBegin(GL_LINES);
		glVertex3f(-xTam,0,zTam);
		glVertex3f(-xTam,yTam,zTam);
	glEnd();
	
	drawTable();
	drawTv();
	drawChaleira();
	
}

void drawScene(){
    glPushMatrix();
    glTranslatef(xObj,0,zObj);
    glRotatef(angR,0,1,0);
    GLfloat larg=3.0,alt=1.0,comp=1.5;
    float y=0.0,z=0.0;
    initMaterials(16);
    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texturas[4]);
    for(int i=0;i<8; i++){
        GLfloat n0 [] = {0.0,0.0,1.0}; 
        glNormal3fv(n0);
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f,0.0f); glVertex3f(larg , 0+y,  z);
            glTexCoord2f(1.0f,0.0f); glVertex3f(larg , alt+y,  z);
            glTexCoord2f(1.0f,1.0f); glVertex3f(-larg , alt+y, z );
            glTexCoord2f(0.0f,1.0f); glVertex3f(-larg , 0+y, z);
        glEnd();
        y+=alt;
        z-=comp;
    }
    for(int i=0;i<8; i++){
        GLfloat n0 [] = {0.0,0.0,-1.0}; 
        glNormal3fv(n0);
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f,1.0f); glVertex3f(larg ,y,  z+comp);
            glTexCoord2f(1.0f,1.0f); glVertex3f(larg , alt+y, z+comp);
            glTexCoord2f(1.0f,0.0f); glVertex3f(3*larg , alt+y, z+comp);
            glTexCoord2f(0.0f,0.0f); glVertex3f(3*larg , y,  z+comp);
        glEnd();
        y+=alt;
        z+=comp;
    }
    y=0.0;
    z=0.0;
    for(int i=0;i<8; i++){
        GLfloat n0 [] = {0.0,1.0,0.0}; 
        glNormal3fv(n0);
        glBegin(GL_QUADS);
            glTexCoord2f(0.0f,0.0f); glVertex3f(larg , alt+y,  z);
            glTexCoord2f(1.0f,0.0f); glVertex3f(larg , alt+y,  -comp+z);
            glTexCoord2f(1.0f,1.0f); glVertex3f(-larg , alt+y, -comp+z );
            glTexCoord2f(0.0f,1.0f); glVertex3f(-larg , alt+y, z);
        glEnd();
        y+=alt;
        z-=comp;
    }
for(int i=0;i<8; i++){
        GLfloat n0 [] = {0.0,1.0,0.0}; 
        glNormal3fv(n0);
        glBegin(GL_QUADS);
             glTexCoord2f(0.0f,1.0f); glVertex3f(larg ,y,  z); 
             glTexCoord2f(1.0f,1.0f); glVertex3f(larg , y,  z+comp);
             glTexCoord2f(1.0f,0.0f); glVertex3f(3*larg ,y, z+comp );
             glTexCoord2f(0.0f,0.0f); glVertex3f(3*larg , y, z);
        glEnd();
        y+=alt;
        z+=comp;
    }
    glDisable(GL_TEXTURE_2D);
    GLfloat n0 [] = {1.0,0.0,0.0}; 
    glNormal3fv(n0);
    glBegin(GL_QUADS);
        glVertex3f(3*larg , 0.0,  0.0);
        glVertex3f(3*larg , 0.0,  -12.0);
        glVertex3f(3*larg, yTam, -12.0);
        glVertex3f(3*larg, yTam,  0.0);
    glEnd();
    glPopMatrix();
}

void display(void){
  	
	 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glViewport (0, 0, wScreen, hScreen);					
	glMatrixMode(GL_PROJECTION);								
	glLoadIdentity();												
	gluPerspective(angZoom, (float)wScreen/hScreen, 0.1, 90.0);			
	glMatrixMode(GL_MODELVIEW);										
	glLoadIdentity();												
	gluLookAt(posicao_observador[0],posicao_observador[1],posicao_observador[2],olhar_observador[0],olhar_observador[1],olhar_observador[2], 0, 1, 0);
	initLights();
	drawEixos(); 
	drawScene(); 
	drawSala();
	
	
	glViewport(0,300,200,200);
	glMatrixMode(GL_PROJECTION);										
	glLoadIdentity();
	glOrtho(xTam,-xTam, -xTam,xTam, zTam,-zTam);
	glMatrixMode(GL_MODELVIEW);												
	glLoadIdentity();
	//gluLookAt(posicao_observador[0],10,posicao_observador[2], posicao_observador[0],4.1 ,posicao_observador[2], 0, 1, 0);
	gluLookAt(posicao_observador[0],-10,posicao_observador[2], posicao_observador[0],0 ,posicao_observador[2], 0, 0, -1);
	drawEixos(); 
	drawScene();
	drawSala(); 	
	glutSwapBuffers();
}

void updateVisao(){
	//Lados
	olhar_observador[0]=posicao_observador[0]+raio_observador*cos(ang_observador);
	olhar_observador[2]=posicao_observador[2]-raio_observador*sin(ang_observador);
	//Cima e baixo
	olhar_observador[1]=posicao_observador[1]+raio_observador*sin(inclinacao_observador);
	
	posFoco[0]=posicao_observador[0];
	posFoco[1]=posicao_observador[1];
	posFoco[2]=posicao_observador[2];
	
	dirFoco[0]=olhar_observador[0]-posicao_observador[0];
	dirFoco[1]=olhar_observador[1]-posicao_observador[1];
	dirFoco[2]=olhar_observador[2]-posicao_observador[2];
	
	
	
	
	glutPostRedisplay();
}


void keyboard(unsigned char key, int x, int y){
	switch (key){
		case 'A':
		case 'a':
			ang_observador+=0.1;
			updateVisao();
			break;
	
		case 'W':
		case 'w':
			if(inclinacao_observador<=0.5*PI)
				inclinacao_observador+=0.05;
			updateVisao();
			break;
	
		case 'S':
		case 's':
			if(inclinacao_observador>=-0.5*PI)
				inclinacao_observador-=0.05;
			updateVisao();
			break;
	
		case 'd':
		case 'D':
			ang_observador-=0.1;
			updateVisao();
			break;
		
		case 'q':
		case 'Q':
			angR-=6.0;
			glutPostRedisplay();
			break;
		
		case 'e':
		case 'E':
			angR+=6.0;
			glutPostRedisplay();
			break;
		case 'r':
		case 'R':
			if(repeat>=5.0)
				repeat = 1.0;
			else
				repeat += 1.0;
			glutPostRedisplay();
			break;
		case 'f':
		case 'F':
			ligaFoco=ligaFoco*(-1);
			glutPostRedisplay();	
			break;
		case 'g':
		case 'G':
			if(luzGlobal[0]>=0.2){
				luzGlobal[0]=0.0;
				luzGlobal[1]=0.0;
				luzGlobal[2]=0.0;	
			}else{
				luzGlobal[0]=0.2;
				luzGlobal[1]=0.2;
				luzGlobal[2]=0.2;
			}
			glutPostRedisplay();
			break;
		case 'p':
		case 'P':
			ligaPontual=ligaPontual*(-1);
			glutPostRedisplay();
			break;
		case 't':
		case 'T':
			ligaTv=ligaTv*(-1);
			glutPostRedisplay();	
			break;
		case '1':
			if(ligaTv>=1)
				canal=1;
			glutPostRedisplay();
			break;
		case '2':
			if(ligaTv>=1){
				canal=2;
				iniParticulas(particula1);
				glutPostRedisplay();
			}
			break;
		case 27:
			exit(0);
			break;	
  }

}



void teclasNotAscii(int key, int x, int y){
	if(key == GLUT_KEY_UP){
		posicao_observador[0]=posicao_observador[0]+0.1*cos(ang_observador);
		posicao_observador[2]=posicao_observador[2]-0.1*sin(ang_observador);			
	}
	if(key == GLUT_KEY_DOWN){
		posicao_observador[0]=posicao_observador[0]-0.1*cos(ang_observador);
		posicao_observador[2]=posicao_observador[2]+0.1*sin(ang_observador);	
	}
	if(key == GLUT_KEY_LEFT){
		posicao_observador[0]=posicao_observador[0]-0.3*cos(ang_observador-0.5*PI);
		posicao_observador[2]=posicao_observador[2]+0.3*sin(ang_observador-0.5*PI);
	}
	if(key == GLUT_KEY_RIGHT){
		posicao_observador[0]=posicao_observador[0]+0.3*cos(ang_observador-0.5*PI);
		posicao_observador[2]=posicao_observador[2]-0.3*sin(ang_observador-0.5*PI);
	}
	updateVisao();	

}

void idle(void)
{

 glutPostRedisplay();

}

void Timer(int value) 
{
	iniParticulas(particula1);
	glutPostRedisplay();
	glutTimerFunc(1500,Timer, 1);
}

int main(int argc, char** argv){

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
	glutInitWindowSize (wScreen, hScreen); 
	glutInitWindowPosition (0, 0); 
	glutCreateWindow ("uc{2016225626,2016226022}@student.dei.uc.pt");
  
	inicializa();
	
	glutSpecialFunc(teclasNotAscii); 
	glutDisplayFunc(display); 
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	glutTimerFunc(1500, Timer, 1);
	glutMainLoop();
	return 0;
}




