﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneticIndividual : Individual {


	public GeneticIndividual(int[] topology) : base(topology) {
	}

	public override void Initialize () 
	{
        for (int i = 0; i < totalSize; i++)
        {
            genotype[i] = Random.Range(-1.0f, 1.0f);
        }
    }
		
	public override void Crossover (Individual partner, float probability)
	{
        int i;
        float randomCrossover= Random.Range(0.0f, 1.0f);
        if (randomCrossover < probability)
        {
            int randomL1 = Random.Range(0, genotype.Length-1);
            int randomL2 = Random.Range(randomL1, genotype.Length-1);
            for (i = randomL1; i <= randomL2; i++)
            {
                genotype[i] = partner.Genotype[i];
            }

        }
	}

	public override void Mutate (float probability)
	{
        int i;
        for (i = 0; i < totalSize; i++)
        {
            float random = Random.Range(0.0f, 1.0f);
            if (random < probability)
            {
                random = Random.Range(-1.0f, 1.0f);
                genotype[i] = random;
            }
        }
    }

	public override Individual Clone ()
	{
		GeneticIndividual new_ind = new GeneticIndividual(this.topology);
		genotype.CopyTo (new_ind.genotype, 0);
		new_ind.fitness = this.Fitness;
		new_ind.evaluated = false;
		return new_ind;
	}

}
