﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GeneticAlgorithm : MetaHeuristic {
    /*public float mutationProbability;
    public float crossoverProbability;
    public int tournamentSize;
    public bool elitist;
    public float elitistPercent;*/
    public SelecaoTorneio selecao;

    public override void InitPopulation() {
        population = new List<Individual>();
        selecao = new SelecaoTorneio();
        while (population.Count < populationSize)
        {
            GeneticIndividual new_ind = new GeneticIndividual(topology);
            new_ind.Initialize();
            population.Add(new_ind);
        }
    }
    //The Step function assumes that the fitness values of all the individuals in the population have been calculated.
    public override void Step() {
        updateReport();
        List<Individual> novaPopulacao = new List<Individual>();
        int i;
       
        for (i = 0; i < populationSize - elitistPercent*populationSize; i++)
        {
            List<Individual> listaSelecao = selecao.selectIndividuals(population, tournamentSize);
            novaPopulacao.Add(listaSelecao[0].Clone());
            novaPopulacao[i].Crossover(listaSelecao[1], crossoverProbability);
            novaPopulacao[i].Mutate(mutationProbability);
        }
        if (elitist)
        {
            population.OrderBy(o => o.Fitness).ToList();
            Debug.Log(population);
            for (i = 0; i < elitistPercent * populationSize; i++)
            {
                novaPopulacao.Add(population[i].Clone());
            }
        }
        population = novaPopulacao;
        //Selecao sobreviventes elitismo
        generation++;
    }

}
