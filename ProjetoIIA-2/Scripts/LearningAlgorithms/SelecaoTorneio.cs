﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelecaoTorneio : SelectionMethod { 


    public SelecaoTorneio()
    {

    }
    override
    public List<Individual> selectIndividuals(List<Individual> oldpop, int num) {
        int i;

        List<int> randoms = new List<int>();
        List<Individual> listaInd = new List<Individual>();
        List<Individual> parents = new List<Individual>();

        for (i = 0; i < num; i++)
        {
            int random = Random.Range(0, oldpop.Count - 1);
            if (!randoms.Contains(random))
            {
                randoms.Add(random);
                listaInd.Add(oldpop[random]);
            }

        }

        GeneticIndividual bestInd = null;
        float maxFit = float.MinValue;
        for (i = 0; i < listaInd.Count; i++)
        {
            if (listaInd[i].Fitness > maxFit)
            {
                bestInd =(GeneticIndividual) listaInd[i];
                maxFit = listaInd[i].Fitness;
            }
        }
        parents.Add(bestInd);
        randoms.Clear();
        listaInd.Clear();
        maxFit = float.MinValue;

        for (i = 0; i < num; i++)
        {
            int random = Random.Range(0, oldpop.Count - 1);
            if (!randoms.Contains(random))
            {
                randoms.Add(random);
                listaInd.Add(oldpop[random]);
            }

        }

        for (i = 0; i < listaInd.Count; i++)
        {
            if (listaInd[i].Fitness > maxFit)
            {
                bestInd = (GeneticIndividual) listaInd[i];
                maxFit = listaInd[i].Fitness;
            }
        }
        parents.Add(bestInd);
        return parents;
    }
}
