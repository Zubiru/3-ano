#include "generatecode.h"
#include "structures.h"
#include "symbol_table.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const char* name_type[] = {"i32","i8","double","void","undef","i1"};
int printState=0;
int numString=4;
char *arrayStrings[800];
int tamanhoStrings[800];
int colocaReturn=1;
/*write some code here*/

char * type(char * s){ 
	if(s==NULL)
		return strdup("void");
	else if(strcmp(s,"Int")==0)
		return strdup("i32");		
	else if(strcmp(s,"Float32")==0)
		return strdup("double");
	else if(strcmp(s,"Bool")==0)
		return strdup("i1");
	else  //string
		return strdup("i8");
}

void generate_program(is_program* myprogram, table* global){
	table* global_table = global->nextFunction; 
	is_declaration_list* aux = myprogram->dlist;
	printf("declare i32 @printf(i8*, ...)\n");
	printf("declare i32 @atoi(i8*)\n");
	printf("@.str = private unnamed_addr constant [4 x i8] c\"%%d\\0A\\00\"\n");
	printf("@.str.1 = private unnamed_addr constant [8 x i8] c\"%%.08lf\\0A\\00\"\n");
	printf("@.str.2 = private unnamed_addr constant [6 x i8] c\"true\\0A\\00\"\n");
	printf("@.str.3 = private unnamed_addr constant [7 x i8] c\"false\\0A\\00\"\n");
	while(aux!=NULL){
		if(aux->valV!=NULL){
			if(strcmp(type(aux->valV->tipo),"double")==0)
				printf("@%s = common global double 0.0\n",aux->valV->id);
			else
				printf("@%s = common global %s 0\n",aux->valV->id,type(aux->valV->tipo));
		}		
		else if(aux->valF!=NULL){
			generate_funcdec(aux->valF,global,global_table);
			global_table = global_table->nextFunction;
		}
		aux = aux->next;
		
	}
}

void generate_funcdec(is_funcdec* funcdec,table * global, table * mytable){
	int num=1;
	colocaReturn=1;
	num=generate_funcheader(funcdec->header,num,mytable);
	if(funcdec->body!=NULL)
		generate_funcbody(funcdec->body,num,global,mytable);
	if(strcmp(mytable->name,"main")==0){
		printf("ret i32 0\n");
	}else{
		if(mytable->returnType==none)
			printf("ret void\n");
		else if(mytable->returnType==float32)
			printf("ret double 0.0\n");
		else
			printf("ret %s 0\n",name_type[mytable->returnType]);
	}
	
	printf("}\n");
	if(printState>0){
		int i;
		for(i=0;i<printState;i++){
			printf("@.str.%d = private unnamed_addr constant [%d x i8] c%s\n",numString-(printState-i),tamanhoStrings[i],arrayStrings[i]); //%s
			free(arrayStrings[i]);
			
		}
		printState=0;
	}
}

int generate_funcheader(is_func_header * header, int num, table* mytable){
	is_func_params * aux = header->parametros;
	if(strcmp(header->id,"main")==0){
		printf("define i32 @main(");		
		if(aux==NULL)
			printf("i32, i8**");
		else
			printf("i32, i8**, ");
		num+=2;
	}else{
		printf("define %s @%s(",type(header->tipo),header->id);	
	}
	while(aux!=NULL){
		if(aux->next==NULL)
			printf("%s",type(aux->tipo));
		else
			printf("%s,",type(aux->tipo));
		aux = aux->next;
		num++;
	}
	printf(") {\n");
	aux = header->parametros;
	int conta=0;
	//saltar sim ou nao ?
	while(aux!=NULL){
		printf("%%%d = alloca %s\n",num,type(aux->tipo));
		printf("store %s %%%d, %s* %%%d\n",type(aux->tipo),conta,type(aux->tipo),num);
		updateParams(mytable->listaParam,aux->id,num);
		conta++;
		num++;
		aux = aux->next;
	}
	return num;
}

void generate_funcbody(is_func_body * funcbody, int num, table* global, table* mytable){
	is_func_body* aux = funcbody;
	while(aux!=NULL){
		if(aux->valV!=NULL)
			num = generate_vardec(aux->valV,global,mytable,num);	
		else if(aux->valS!=NULL)
			num = generate_statement(aux->valS,global,mytable,num);
		aux = aux->next;
	}
}

int generate_vardec(is_vardec* vardec, table* global, table* mytable, int num){
	printf("%%%d = alloca %s\n",num,type(vardec->tipo));
	updateFuncVar(mytable->nextDecl,vardec->id,num);
	num++;
	return num;
}

int generate_StateList(is_statelist* statelist,table* global,table* mytable, int num){
	num=generate_statement(statelist->statement,global,mytable,num);
	if(statelist->next!=NULL)
		num = generate_StateList(statelist->next,global,mytable,num);
	return num;
}


int generate_statement(is_statement* statement,table* global, table* mytable, int num){
	if(strcmp(statement->identificador,"Assign")==0){
		num = generate_exp(statement->node,num,mytable);
		int temp = getNum(mytable,statement->id); 
		if(temp!=-1)	
			printf("store %s %%%d, %s* %%%d\n",name_type[statement->node->type], num-1 , name_type[statement->node->type], temp);
		else
			printf("store %s %%%d, %s* @%s\n",name_type[statement->node->type], num-1 , name_type[statement->node->type], statement->id);
	}
	else if(strcmp(statement->identificador,"Return")==0){
		if(statement->node!=NULL){
			num = generate_exp(statement->node,num,mytable);
			printf("ret %s %%%d\n",name_type[statement->node->type],num-1);		
		}else{
			printf("ret void\n");	
		}
		/*colocaReturn=0;*/ /*cuidado se estiver dentro de um if nao garante o return */
 		num++;
	}
	else if(strcmp(statement->identificador,"funcinvocation")==0){
		num = generate_exp(statement->node,num,mytable);		
	}
	else if(strcmp(statement->identificador,"PrintExp")==0){
		num = generate_exp(statement->node,num,mytable);
		if(statement->node->type==integer){
			printf("%%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 %%%d)\n",num, num-1);
			num++;
		}else if(statement->node->type==float32){
			printf("%%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.1, i32 0, i32 0), double %%%d)\n",num, num-1);
			num++;
		}
		//fazer if para escolher printar true or false
		else if(statement->node->type==boolean){
			printf("%%%d = icmp eq i1 %%%d,0\n",num,num-1);
			printf("br i1 %%%d, label %%IfEqual%d, label %%IfUnequal%d\n",num,num,num);
			printf("IfEqual%d:\n",num);
			printf("\t%%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.3, i32 0, i32 0))\n",num+1); 		
			printf("br label %%OutIf%d\n",num);
			printf("IfUnequal%d:\n",num);
			printf("\t%%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.2, i32 0, i32 0))\n",num+2); 		
			printf("br label %%OutIf%d\n",num);
			printf("OutIf%d:\n",num);
			num+=3;	
		}
	}
	else if(strcmp(statement->identificador,"Print")==0){
		//Cuidado tamanho String
		char * stringAux=(char*) malloc(sizeof(char) *800);
		int j=0;
		int tamanhoString=0;
		while(statement->id[j]!='\0'){
			if(statement->id[j]=='\\'){
				j++;
				if(statement->id[j]=='n')
					strcat(stringAux,"\\0A");
				if(statement->id[j]=='t')
					strcat(stringAux,"\\09");
				if(statement->id[j]=='r')
					strcat(stringAux,"\\0D");
				if(statement->id[j]=='f')
					strcat(stringAux,"\\0C");
				if(statement->id[j]=='"')
					strcat(stringAux,"\\22");
				if(statement->id[j]=='\\')
					strcat(stringAux,"\\5C");
				tamanhoString++;
			}else if(statement->id[j]=='%'){
					strcat(stringAux,"%%");
					tamanhoString+=2;				
				}
			else{
				if(statement->id[j]=='"' && j>0){
					strcat(stringAux,"\\0A");
					strcat(stringAux,"\\00");
					tamanhoString+=2;
				}
				strncat(stringAux,&(statement->id[j]),1);
				tamanhoString++;					
			}
			j++;
		}
		tamanhoString-=2;
		printf("\t%%%d = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([%d x i8], [%d x i8]* @.str.%d, i32 0, i32 0))\n",num,tamanhoString,tamanhoString,numString);
		arrayStrings[printState]=stringAux;
		tamanhoStrings[printState]=tamanhoString;
		printState++;
		numString++;
		num++;	
	}
	else if(strcmp(statement->identificador,"Block")==0){
		if(statement->next!=NULL)
			num = generate_StateList(statement->next,global,mytable,num);
	}else if(strcmp(statement->identificador,"If")==0){
		num = generate_exp(statement->node,num,mytable);
		int labelNum=num-1;
		if(statement->elseNext!=NULL){
			printf("br i1 %%%d, label %%IfEqual%d, label %%IfUnequal%d\n",labelNum,labelNum,labelNum);
			printf("IfEqual%d:\n",labelNum);
			if(statement->next!=NULL){
				num = generate_StateList(statement->next,global,mytable,num);
			}		
			printf("br label %%OutIf%d\n",labelNum);
			printf("IfUnequal%d:\n",labelNum);
			num = generate_StateList(statement->elseNext,global,mytable,num);
			printf("br label %%OutIf%d\n",labelNum);
			printf("OutIf%d:\n",labelNum);
		}else{
			printf("br i1 %%%d, label %%IfEqual%d, label %%OutIf%d\n",labelNum,labelNum,labelNum);
			printf("IfEqual%d:\n",labelNum);
			if(statement->next!=NULL){
				num = generate_StateList(statement->next,global,mytable,num);
			}		
			printf("br label %%OutIf%d\n",labelNum);
			printf("OutIf%d:\n",labelNum);
		}

	}else if(strcmp(statement->identificador,"For")==0){
		if(statement->node!=NULL){
			int labelCiclo=num-1;
			printf("br label %%inicioCiclo%d\n",labelCiclo);
			printf("inicioCiclo%d:\n",labelCiclo);
			num = generate_exp(statement->node,num,mytable);
			int labelNum=num-1;
			printf("br i1 %%%d, label %%IfTrue%d, label %%OutFor%d\n",labelNum,labelNum,labelNum);
			printf("IfTrue%d:\n",labelNum);
			if(statement->next!=NULL)
				num = generate_StateList(statement->next,global,mytable,num);
			printf("br label %%inicioCiclo%d\n",labelCiclo);
			printf("OutFor%d:\n",labelNum);	
		}else{  //Ciclo infinito????
			int labelNum=num-1;
			printf("br label %%inicioCiclo%d\n",labelNum);
			printf("inicioCiclo%d:\n",labelNum);
			if(statement->next!=NULL)
				num = generate_StateList(statement->next,global,mytable,num);
			printf("br label %%inicioCiclo%d\n",labelNum);
		}
	}else if(strcmp(statement->identificador,"ParseArgs")==0){
		printf("%%%d = alloca i8**\n",num);
		num++;
		printf("%%%d = alloca i32\n",num);
		num++;
		printf("store i32 %%0, i32* %%%d\n",num-1);
		printf("store i8** %%1, i8*** %%%d\n",num-2);
		printf("%%%d = load i8**, i8*** %%%d\n",num,num-2);
		num++;
		int auxNum=num-1;
		num = generate_exp(statement->node,num,mytable);
		printf("%%%d = getelementptr inbounds i8*, i8** %%%d, i32 %%%d\n",num,auxNum,num-1);
		num++;
		printf("%%%d = load i8*, i8** %%%d\n",num,num-1);
		num++;
		printf("%%%d = call i32 @atoi(i8* %%%d)\n",num,num-1);
		num++;
		int temp = getNum(mytable,statement->id); 
		if(temp!=-1)	
			printf("store %s %%%d, %s* %%%d\n",name_type[statement->node->type], num-1 , name_type[statement->node->type], temp);
		else
			printf("store %s %%%d, %s* @%s\n",name_type[statement->node->type], num-1 , name_type[statement->node->type], statement->id);
	}	
	return num;
}

int generate_exp(is_nodeExp* head,int temp, table* mytable){
	is_nodeExp* aux= head;
	if(aux->operation!=NULL){
		if(strcmp(aux->operation,"next")==0){
			return generate_exp(aux->right,temp,mytable);
		}
		if(strcmp(aux->operation,"Call")==0){
			int nParams=0;
			is_nodeExp* param = aux->left;
			while(param!=NULL){
				nParams++;
				param = param->left;
			}
			int temps[nParams];
			nParams = 0;
			param = aux->left;
			while(param!=NULL){
				temp = generate_exp(param,temp,mytable);
				//GUARDAR NO ARRAY TEMPS
				temps[nParams]=temp-1;
				param = param->left;
				nParams++;
			}
			if(aux->type!=none)
				printf("%%%d = call %s @%s(",temp,name_type[aux->type],aux->num->numero);
			else
				printf("call %s @%s(",name_type[aux->type],aux->num->numero);
			param = aux->left;
			nParams = 0;
			while(param!=NULL){
				if(param->left!=NULL)
					printf("%s %%%d,",name_type[param->right->type],temps[nParams]);
				else
					printf("%s %%%d",name_type[param->right->type],temps[nParams]);
				param = param->left;
				nParams++;
			}
			printf(")\n");
			return temp+1;
		}
	
	}
	else if(aux->operation==NULL){
		if(strcmp(aux->num->tipo,"Id")==0){ //procurar e dar load
			int res = getNum(mytable,aux->num->numero);	
			if(res!=-1)
				printf("%%%d = load %s, %s* %%%d\n",temp,name_type[aux->type],name_type[aux->type],res);
			else
				printf("%%%d = load %s, %s* @%s\n",temp,name_type[aux->type],name_type[aux->type],aux->num->numero);
		}else{
			if(strcmp(aux->num->tipo,"IntLit")==0){
				printf("%%%d = add i32 0,%s\n",temp,aux->num->numero);
			}else{
				if(aux->num->numero[0]=='.')
					printf("%%%d = fadd double 0.0,0%s\n",temp,aux->num->numero); 
				else{
					printf("%%%d = fadd double 0.0,",temp);
					int i;
					int tam = strlen(aux->num->numero);
					int ponto = 0;
					for(i=0;i<tam;i++){
						if(aux->num->numero[i]=='.')
							ponto = 1;
						if(aux->num->numero[i]=='e' || aux->num->numero[i]=='E'){
							if(!ponto){
								printf(".");
							}
							
						}
						printf("%c",aux->num->numero[i]);				
					}
					printf("\n");
					
				} 
			}
		}
		return temp+1;
	}
	int esquerda=0,direita=0;
	if(aux->left!=NULL){
		temp=generate_exp(aux->left,temp,mytable);
		esquerda = temp-1;

	}
	if(aux->right!=NULL){
		temp=generate_exp(aux->right,temp,mytable);
		direita = temp-1; 
	}
	temp = generate_operators(aux,aux->type,temp,esquerda,direita);
	return temp;
	
}

int generate_operators(is_nodeExp* aux,basic_type tipo,int temp,int esquerda, int direita){
	if(strcmp("Not",aux->operation)==0){
		printf("%%%d = icmp eq i1 %%%d,0\n",temp,esquerda); //se for false
		return temp+1;
	}
	if(strcmp("Minus",aux->operation)==0){
		if(tipo==integer){
			printf("%%%d = mul i32 -1,%%%d\n",temp,esquerda);
		}
		else if(tipo==float32){
			printf("%%%d = fmul double -1.0,%%%d\n",temp,esquerda);
		}
		return temp+1;	
	}
	if(strcmp("Plus",aux->operation)==0){
		return temp;
	}
	if(strcmp("Or",aux->operation)==0){
		printf("%%%d = or i1 %%%d,%%%d\n",temp,esquerda,direita);
		return temp +1;	
	}
	if(strcmp("And",aux->operation)==0){
		printf("%%%d = and i1 %%%d,%%%d\n",temp,esquerda,direita);
		return temp +1;	
	}
	if(strcmp("Lt",aux->operation)==0){
		if(aux->left->type==integer){
			printf("%%%d = icmp slt i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==float32){
			printf("%%%d = fcmp olt double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Gt",aux->operation)==0){
		if(aux->left->type==integer){
			printf("%%%d = icmp sgt i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==float32){
			printf("%%%d = fcmp ogt double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Eq",aux->operation)==0){ //posso usar bool
		if(aux->left->type==integer){
			printf("%%%d = icmp eq i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==float32){
			printf("%%%d = fcmp oeq double %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==boolean){
			printf("%%%d = icmp eq i1 %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Ne",aux->operation)==0){
		if(aux->left->type==integer){
			printf("%%%d = icmp ne i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==float32){
			printf("%%%d = fcmp one double %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==boolean){
			printf("%%%d = icmp ne i1 %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Le",aux->operation)==0){
		if(aux->left->type==integer){
			printf("%%%d = icmp sle i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==float32){
			printf("%%%d = fcmp ole double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Ge",aux->operation)==0){
		if(aux->left->type==integer){
			printf("%%%d = icmp sge i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(aux->left->type==float32){
			printf("%%%d = fcmp oge double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Add",aux->operation)==0){ 
		if(tipo==integer){
			printf("%%%d = add i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(tipo==float32){
			printf("%%%d = fadd double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Sub",aux->operation)==0){
		if(tipo==integer){
			printf("%%%d = sub i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(tipo==float32){
			printf("%%%d = fsub double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;	
	}
	if(strcmp("Mul",aux->operation)==0){
		if(tipo==integer){
			printf("%%%d = mul i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(tipo==float32){
			printf("%%%d = fmul double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Div",aux->operation)==0){
		if(tipo==integer){
			printf("%%%d = sdiv i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(tipo==float32){
			printf("%%%d = fdiv double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	if(strcmp("Mod",aux->operation)==0){
		if(tipo==integer){
			printf("%%%d = srem i32 %%%d,%%%d\n",temp,esquerda,direita);
		}
		else if(tipo==float32){
			printf("%%%d = frem double %%%d,%%%d\n",temp,esquerda,direita);
		}
		return temp+1;
	}
	return undef;
}
