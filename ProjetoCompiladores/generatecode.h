#include "structures.h"
#include "symbol_table.h"

//Declare func headers here
char * type(char * s);
void generate_program(is_program* myprogram, table* global_table);
void generate_funcdec(is_funcdec* funcdec,table * global, table * mytable);
int generate_funcheader(is_func_header * head,int num,table * mytable);
void generate_funcbody(is_func_body * funcbody, int num, table* global, table* mytable);
int generate_vardec(is_vardec* vardec, table* global, table* mytable, int num);
int generate_operators(is_nodeExp* aux,basic_type tipo,int temp,int esquerda, int direita);
int generate_exp(is_nodeExp* head,int temp,table* mytable);
int generate_statement(is_statement* statement,table* global, table* mytable, int num);
int generate_StateList(is_statelist* statelist,table* global,table* mytable, int num);
