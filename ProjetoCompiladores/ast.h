#include "structures.h"

/*declare ast functions*/
is_program* insert_program(is_declaration_list* idl);
//is_vardec* createNodeVar(char* id, char* tipo);
//is_declaration_list* createNodeVarDec(is_vardec* iv);
is_declaration_list* insertDeclarations(is_declaration_list* node, is_declaration_list* head);
is_id_list* insertIds(char *id, is_id_list* head,int linha,int coluna);
is_declaration_list* createNodeVar(char* id, char* tipo,is_id_list* listIds,int linha,int coluna);
is_func_params* insertParams(char* id,char* tipo, is_func_params* head,int linha,int coluna);
is_func_header* createNodeHeader(char* id, char* tipo, is_func_params* params,int linha,int coluna);
is_declaration_list* createNodeFuncDec(is_func_header* header, is_func_body* body);
is_func_body* insertBodyVar(is_declaration_list* idl, is_func_body* head);
is_number* createNodeNumber(char* num, char* tipo);
is_nodeExp* createNodeTree(is_number* num,int linha,int coluna);
is_nodeExp* insertNodeTree(is_nodeExp* exp1,is_nodeExp* exp2,char* operation,int linha,int coluna);
is_nodeExp* createSpecialNode(is_nodeExp* node,char* id,char* tipo);
is_nodeExp* insertSpecialNode(is_nodeExp* head,is_nodeExp* exp);
is_nodeExp* createCallNode(is_nodeExp* exp1,char* id,int linha, int coluna);
//is_statement* createStatement(char* identificador, is_nodeExp* node, is_statement* next,is_statement* elseNext,char* id);
//is_statement* insertStatement(is_statement* s1,is_statement* s2);
is_statelist* insertStatement(is_statement* novoState,is_statelist* head);
is_func_body* insertBodyState(is_statement* statement,is_func_body* head);
is_statement* createStatement(char* identificador, is_nodeExp* node,is_statelist* next,is_statelist* elseNext,char* id);
is_statement* createAssignState(char* identificador, is_nodeExp* node,is_statelist* next,is_statelist* elseNext,char* id,int linhaId,int colunaId,int linhaAssign,int colunaAssign);
int tamanhoBloco(is_statelist* lista);
is_statement* createStatementReturn(char* identificador, is_nodeExp* node,is_statelist* next,is_statelist* elseNext,char* id,int linha, int coluna);

//Prints
void printPontos(int numeroPontos);
void printVarDeclaration(is_vardec* vardec,int numeroPontos);
void printFuncDeclaration(is_funcdec * funcdec,int numeroPontos,int anotada);
void printHeader(is_func_header *head,int numeroPontos);
void printParam(is_func_params* params, int numeroPontos);
void printBody(is_func_body* body, int numeroPontos,int anotada);
void printExp(is_nodeExp* head,int numeroPontos,int anotada);
void printStatement(is_statement* statement, int numeroPontos,int anotada);
void printStateList(is_statelist* statelist,int numeroPontos,int anotada);


//Liberta Memoria
void freeVarDeclaration(is_vardec* vardec);
void freeFuncDeclaration(is_funcdec * funcdec);
void freeParam(is_func_params* params);
void freeHeader(is_func_header *head);
void freeBody(is_func_body* body);
void freeStateList(is_statelist* statelist);
void freeStatement(is_statement* statement);
void freeExp(is_nodeExp* head);
	
