#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

//declare structs and function calls here
typedef struct _t1 table;
typedef enum{
	integer,
	string,
	float32,
	none,
	undef,
	boolean,
} basic_type;

typedef struct struct_param{
	char* id;
	int temp;
	basic_type type;
	struct struct_param * next;
}paramList;

typedef struct _t2{
	int isVarDec;
	char* name;                                
	basic_type type;                                 
	table* funcTable;
	struct _t2* next;
	struct _t2* previous;  //new
	int open_close;  // 0   -1 - fechar block   +1 - abres bloco
	int linha;
	int coluna;
	int used;
	int temp;
} table_element;

typedef struct _t1{
	char* name;
	paramList *listaParam;
	basic_type returnType;
	struct _t1* nextFunction; //funções
	table_element* nextDecl; //declaraçoes
	table_element* previous; 
} table;


int insertVarDec(table* tab,char* name,basic_type type,int linha,int coluna);
table* insertFuncDec(table* tab,char* name,basic_type type,paramList *param);
void insertOpenClose(table* tabela,int open_close);
void updateParams(paramList* parametros, char * id, int num);
void updateFuncVar(table_element* var, char *id, int num);
int getNum(table* mytable, char *id);	

#endif
