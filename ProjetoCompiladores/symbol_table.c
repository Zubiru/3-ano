#include "symbol_table.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//extern table_element* symtab;

//functions to insert, search,... here

int insertVarDec(table* tab,char* name,basic_type type,int linha,int coluna){
	table_element* head = tab->nextDecl;
	table_element* new_element = (table_element*)malloc(sizeof(table_element));
	new_element->name=(char*)strdup(name);
	new_element->type=type;
	new_element->isVarDec=1;
	new_element->next=NULL;
	new_element->previous=NULL;
	new_element->open_close = 0;
	new_element->used = 0;
	new_element->linha = linha;
	new_element->coluna = coluna;
	if(head==NULL){
		head=new_element;
		tab->nextDecl = head;		
	}else{
		while(head->next!=NULL){
			head=head->next;		
		}
		head->next = new_element;
		new_element->previous = head;	
	}

	return 1;	
}

table* insertFuncDec(table* tab,char* name,basic_type type,paramList *param){
	//Criar nova Tabela com params
	table * newTable=(table *) malloc(sizeof(table));
	newTable->name=(char*)strdup(name);
	newTable->returnType=type;
	newTable->nextFunction=NULL;
	newTable->listaParam=param;
	newTable->previous=NULL;

	//Criar Declarações dentro da função
	newTable->nextDecl=NULL;

	//Criar NewElement
	table_element* head = tab->nextDecl;
	table_element* new_element = (table_element*)malloc(sizeof(table_element));
	new_element->name=newTable->name;
	new_element->type=type;
	new_element->isVarDec=0;
	new_element->next=NULL;
	new_element->previous=NULL;
	new_element->open_close = 0;
	new_element->funcTable=newTable;
	new_element->used = 0;

	//Inserir NewElement
	if(head==NULL){
		head=new_element;
		tab->nextDecl = head;		
	}else{
		while(head->next!=NULL){
			head=head->next;		
		}
		head->next = new_element;
		new_element->previous = head;	
	}
	//Inserir NovaTabela
	table *aux= tab;
	while(aux->nextFunction!=NULL){
		aux=aux->nextFunction;
	}
	aux->nextFunction=newTable;
	newTable->previous=new_element;
	return newTable;
}

void insertOpenClose(table* tabela,int open_close){
	table_element* new_element = (table_element*)malloc(sizeof(table_element));
	new_element->open_close = open_close;
	new_element->isVarDec=0;
	new_element->next=NULL;
	new_element->previous=NULL;
	
	table_element* head = tabela->nextDecl;
	if(head==NULL){
		head = new_element;
		tabela->nextDecl = head;	
	}else{
		while(head->next!=NULL){
			head=head->next;		
		}
		head->next = new_element;
		new_element->previous = head;	
	}
}

void updateParams(paramList* parametros, char * id, int num){
	paramList* params = parametros;
	while(params!=NULL){
		if(strcmp(id,params->id)==0){
			params->temp = num;
			break;
		}
		params = params->next;
	}
}

void updateFuncVar(table_element* var, char *id, int num){
	table_element* aux = var;	
	while(aux!=NULL){
		if(aux->isVarDec==1 && strcmp(aux->name,id)==0){
			aux->temp = num;
			break;		
		}
		aux = aux->next;
	}
}

int getNum(table* mytable, char *id){
	//procurar nos params
	paramList* params = mytable->listaParam;
	while(params!=NULL){
		if(strcmp(id,params->id)==0){
			return params->temp;
		}
		params = params->next;
	}
	//procurar no corpo
	table_element* aux = mytable->nextDecl;
	while(aux!=NULL){
		if(aux->isVarDec==1 && strcmp(aux->name,id)==0){
			return aux->temp;	
		}
		aux = aux->next;
	}
	return -1;	
}
