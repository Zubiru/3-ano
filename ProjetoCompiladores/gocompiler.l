%option nounput
%option noinput
%{
#include "structures.h"
#include "ast.h"
#include "symbol_table.h"
#include "semantics.h"
#include "y.tab.h"
# define YYLLOC_DEFAULT(Cur, Rhs, N)
int linhaIni;
int colIni;
int column=1;
int line=1;
int flag_l=0;
int state=0;
int strstate=0;
char aux[200];

extern is_program* myprogram;

void arvore();
void arvoreAnotada();
void copy(char* text,int l, int c);
void yyerror(char *s);
void libertaMem();

char* cache;
int errorCol=1;
int errorLine=1;
int noErrors=0;
%}
identificador [a-zA-Z_][a-zA-Z_0-9]*

decimal_lit    [1-9][0-9]*
octal_lit     0[0-7]*
hex_lit       0[xX][0-9A-Fa-f]+

exp	      [eE][+-]?[0-9]+
real_lit_0    [0-9]+"."[0-9]*
real_lit_1    "."[0-9]+    
real_lit_2    [0-9]+
semicolon     ;

seq_escape    \\[^fnrt]
caso_especial \\\"|\\\\
aspasDuplas   \"
teste         \\

comment_1     \/\/                
comment_2     \/"*"
comment_3     "*"\/
  
reservadas    "++"|"--"|"break"|"default"|"interface"|"select"|"case"|"defer"|"go"|"map"|"struct"|"chan"|"goto"|"switch"|"const"|"fallthrough"|"range"|"type"|"continue"|"import"	

%x STRINGSTATE COMMENTSTATE_1 COMMENTSTATE_2

%%
"\n"                   {if(state==1){copy("\n",line,column+1);errorLine=line;errorCol=column;line++;column=1;if(flag_l==1)printf("SEMICOLON\n");state=0;if(flag_l==0)return SEMICOLON;}else{copy(yytext,line,column);line++;column=1;}}
" "                    	{copy(yytext,line,column);column++;}
"\t" 			{copy(yytext,line,column);column+=yyleng;}

"return"                {copy(yytext,line,column);state=1;column+=yyleng;if(flag_l==1)printf("RETURN\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return RETURN;}}
")"			{copy(yytext,line,column);state=1;column+=yyleng;if(flag_l==1)printf("RPAR\n");if(flag_l==0)return RPAR;}
"]"			{copy(yytext,line,column);state=1;column+=yyleng;if(flag_l==1)printf("RSQ\n");if(flag_l==0)return RSQ;}
"}"			{copy(yytext,line,column);state=1;column+=yyleng;if(flag_l==1)printf("RBRACE\n");if(flag_l==0)return RBRACE;}
"_"			{copy(yytext,line,column);state=0;column+=yyleng;if(flag_l==1)printf("BLANKID\n");if(flag_l==0)return BLANKID;}
"package"		{copy(yytext,line,column);state=0;column+=yyleng;if(flag_l==1)printf("PACKAGE\n");if(flag_l==0)return PACKAGE;}
"&&"			{copy(yytext,line,column);state=0;column+=yyleng;if(flag_l==1)printf("AND\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return AND;}}//
"="			{copy(yytext,line,column);state=0;column+=yyleng;if(flag_l==1)printf("ASSIGN\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return ASSIGN;}}//
"*"			{copy(yytext,line,column);state=0;column+=yyleng;if(flag_l==1)printf("STAR\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return STAR;}}//
","			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("COMMA\n");if(flag_l==0)return COMMA;}
"/"			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("DIV\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return DIV;}}//
"=="			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("EQ\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return EQ;}}//
">="			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("GE\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return GE;}}//
">"			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("GT\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return GT;}}//
"{"			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("LBRACE\n");if(flag_l==0)return LBRACE;}
"<="			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("LE\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return LE;}}//
"("			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("LPAR\n");if(flag_l==0)return LPAR;}
"["			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("LSQ\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return LSQ;}}//
"<"			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("LT\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return LT;}}//
"-"			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("MINUS\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return MINUS;}}//
"%"			{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("MOD\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return MOD;}}//
"!="            	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("NE\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return NE;}}//
"!"            		{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("NOT\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return NOT;}}//
"||"           		{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("OR\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return OR;}}//
"+"             	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("PLUS\n");if(flag_l==0){yylloc.first_line=errorLine;yylloc.first_column=errorCol;return PLUS;}}//
"else"          	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("ELSE\n");if(flag_l==0)return ELSE;}
"for"           	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("FOR\n");if(flag_l==0)return FOR;}
"if"            	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("IF\n");if(flag_l==0)return IF;}
"var"           	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("VAR\n");if(flag_l==0)return VAR;}
"int"           	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("INT\n");if(flag_l==0)return INT;}
"float32"       	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("FLOAT32\n");if(flag_l==0)return FLOAT32;}
"bool"          	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("BOOL\n");if(flag_l==0)return BOOL;}
"string"        	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("STRING\n");if(flag_l==0)return STRING;}
"fmt.Println"   	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("PRINT\n");if(flag_l==0)return PRINT;}
"strconv.Atoi"  	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("PARSEINT\n");if(flag_l==0){yylloc.first_column=errorCol;yylloc.first_line=errorLine;return PARSEINT;}}
"func"          	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("FUNC\n");if(flag_l==0)return FUNC;}
"os.Args" 	    	{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("CMDARGS\n");if(flag_l==0)return CMDARGS;}
{reservadas}		{state=0;copy(yytext,line,column);column+=yyleng;if(flag_l==1)printf("RESERVED(%s)\n",yytext);if(flag_l==0)return RESERVED;}

{aspasDuplas}                                  {copy(yytext,line,column);colIni=column;BEGIN STRINGSTATE;column++;strcpy(aux,yytext);}
<STRINGSTATE>{
	{aspasDuplas}                    {column++;BEGIN 0;if(strstate==0){strcat(aux,yytext);if(flag_l==1)printf("STRLIT(%s)\n",aux);copy(aux,line,colIni);state=1;if(flag_l==0){yylval.id=(char*)strdup(aux);return STRLIT;}}strstate=0;}
	{caso_especial}                   {column+=yyleng;strcat(aux,yytext);}
	{teste}"\n"		  {printf("Line %d, column %d: invalid escape sequence (%c)\n",line,column,yytext[0]);column+=yyleng;strstate=0;printf("Line %d, column %d: unterminated string literal\n",line,colIni);line++;column=1;BEGIN 0;}
	{seq_escape}	                  {printf("Line %d, column %d: invalid escape sequence (%s)\n",line,column,yytext);column+=yyleng;strstate=1;}
	"\n"                    	  {printf("Line %d, column %d: unterminated string literal\n",line,colIni);line++;column=1;BEGIN 0;strstate=0;}
	<<EOF>>                       	  {printf("Line %d, column %d: unterminated string literal\n",line,colIni);line++;column=1;BEGIN 0;strstate=0;}
	.                                 {column++;strcat(aux,yytext);}
}
{comment_1}		{BEGIN COMMENTSTATE_1;}
<COMMENTSTATE_1>{
	"\n"				{BEGIN 0;if(state==1){copy(";",line,column+1);if(flag_l==1)printf("SEMICOLON\n");state=0;if(flag_l==0)return SEMICOLON;}line++;column=1;}
	.*
}
{comment_2}		{BEGIN COMMENTSTATE_2;colIni=column;linhaIni=line;column+=yyleng;}
<COMMENTSTATE_2>{
	{comment_3}			{BEGIN 0;column+=yyleng;}
	<<EOF>>			        {printf("Line %d, column %d: unterminated comment\n",linhaIni,colIni);BEGIN 0;}
	.				{column+=yyleng;}
	"\n"				{line++;column=1;}
}

{semicolon}                                                    {copy(yytext,line,column);if(flag_l==1)printf("SEMICOLON\n");column+=yyleng;state=0;if(flag_l==0)return SEMICOLON;}
{identificador}                                 {copy(yytext,line,column);if(flag_l==1)printf("ID(%s)\n",yytext);column+=yyleng;state=1;if(flag_l==0){yylval.id=(char*)strdup(yytext); yylloc.first_column=errorCol;yylloc.first_line=errorLine;return ID;}}//
{decimal_lit}|{octal_lit}|{hex_lit}|{real_lit_2}               {copy(yytext,line,column);if(flag_l==1)printf("INTLIT(%s)\n",yytext);column+=yyleng;state=1;if(flag_l==0){yylval.id=(char*)strdup(yytext);yylloc.first_line=errorLine;yylloc.first_column=errorCol;return INTLIT;}}//
({real_lit_0}{exp}?)|({real_lit_1}{exp}?)|({real_lit_2}{exp}?)  {copy(yytext,line,column);if(flag_l==1)printf("REALLIT(%s)\n",yytext);column+=yyleng;state=1;if(flag_l==0){yylval.id=(char*)strdup(yytext);yylloc.first_line=errorLine;yylloc.first_column=errorCol;return REALLIT;}}//
<<EOF>>	{if(state==1){copy(";",line,column+1);errorLine=line;errorCol=column;line++;column=1;if(flag_l==1)printf("SEMICOLON\n");state=0;if(flag_l==0)return SEMICOLON;}return 0;}
.			{copy(yytext,line,column);state=0;printf("Line %d, column %d: illegal character (%s)\n",line,column,yytext);column+=yyleng;}


%%

int main(int argc, char *argv[])
{
	if(argc==2 && strncmp(argv[1],"-l",2)==0){
		flag_l=1;
		yylex();
	}else{
		yyparse();	
	}

	if(argc==2 && noErrors==0 && strncmp(argv[1],"-t",2)==0){
		if(myprogram!=NULL)
			arvore();
	}
	if(noErrors==0 && myprogram!=NULL){
		if(argc==2 && strncmp(argv[1],"-s",2)==0){
			noErrors=check_program(myprogram,1);
			if(noErrors==0)
				arvoreAnotada();
		}
		else
			noErrors=check_program(myprogram,0);	
	}
	if(myprogram!=NULL)
		//libertaMem();
		
	return 0;
}
int yywrap()
{
  return 1;
}

void arvore(){
	is_declaration_list* aux=myprogram->dlist;
	printf("Program\n");
	while(aux!=NULL){
		if(aux->valF!=NULL){
			printFuncDeclaration(aux->valF,2,0);
		}	
		else if(aux->valV!=NULL){
			printVarDeclaration(aux->valV,2);
		}
	aux=aux->next;		
	}
}
void arvoreAnotada(){
	printf("\n");
	is_declaration_list* aux=myprogram->dlist;
	printf("Program\n");
	while(aux!=NULL){
		if(aux->valF!=NULL){
			printFuncDeclaration(aux->valF,2,1);
		}	
		else if(aux->valV!=NULL){
			printVarDeclaration(aux->valV,2);
		}
	aux=aux->next;		
	}
}
void copy(char* text,int l, int c){
	cache=(char*)strdup(text);
	errorCol=c;
	errorLine=l;
}

void libertaMem(){
	is_declaration_list* aux;
	while(myprogram->dlist!=NULL){
		if(myprogram->dlist->valF!=NULL){
			freeFuncDeclaration(myprogram->dlist->valF);
			free(myprogram->dlist->valF);
		}	
		else if(myprogram->dlist->valV!=NULL){
			freeVarDeclaration(myprogram->dlist->valV);
			free(myprogram->dlist->valV);
		}
	aux=myprogram->dlist;
	myprogram->dlist=myprogram->dlist->next;
	free(aux);	
	}
	free(myprogram);
}
void yyerror(char *s){
	noErrors=1;
	if(strcmp(cache,"\n")==0)
		printf("Line %d, column %d: %s: %s",errorLine,errorCol,s,cache);
	else	
		printf("Line %d, column %d: %s: %s\n",errorLine,errorCol,s,cache);
}
