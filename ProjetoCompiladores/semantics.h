#include "structures.h"
#include "symbol_table.h"

//declare semantic functions here

int check_program(is_program* myprogram,int printar);
int check_varDeclaration(table* tab,is_vardec* var);
int check_varDeclarationFunc(table* tabela, is_vardec* var);
int check_funcDeclaration(table* tab,is_funcdec* funcdec);
int check_statement(table* global, table* tabela, is_statement* statement);
int check_assign(table* global,table* tabela, is_statement* statement);
table_element* check_exists_var(table* tabela, char *id);
basic_type check_types(is_nodeExp* aux,basic_type left, basic_type right,int* erros);
basic_type check_expression(table* global,table* tabela, is_nodeExp* node,int* erros);
int check_funcBody(table* global,table* tabela, is_funcdec* funcdec);
basic_type getType(char *str);
int check_octal(char *numero);
int check_stateList(table* global, table* tabela, is_statelist* statelist);
void printTables(table* tab);
int check_parseargs(table* global, table* tabela, is_statement* statement);
