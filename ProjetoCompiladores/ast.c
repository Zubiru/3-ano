#include "structures.h"
#include "ast.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

extern const char* name_types[]; //= {"int","string","float32","none","undef","bool"};

is_program* insert_program(is_declaration_list* idl){
	is_program* ip = (is_program*)malloc(sizeof(is_program));
	ip->dlist=idl;
	return ip;
}

is_declaration_list* createNodeVar(char* id, char* tipo,is_id_list* listIds,int linha,int coluna){
	is_declaration_list* idl = (is_declaration_list*)malloc(sizeof(is_declaration_list));
	idl->valV = (is_vardec*)malloc(sizeof(is_vardec));
	idl->valV->id = (char*)strdup(id);
	idl->valV->tipo = tipo;
	idl->valF = NULL;
	idl->next = NULL;
	idl->valV->linha=linha;
	idl->valV->coluna=coluna;
	
	is_declaration_list* aux = idl;
	while(listIds!=NULL){
		is_declaration_list* nodeDec = (is_declaration_list*)malloc(sizeof(is_declaration_list));
		nodeDec->valV = (is_vardec*)malloc(sizeof(is_vardec));
		nodeDec->valV->id = (char*)strdup(listIds->id);
		nodeDec->valV->tipo = tipo;
		nodeDec->valV->linha=listIds->linha;
		nodeDec->valV->coluna=listIds->coluna;
		nodeDec->valF = NULL;
		nodeDec->next = NULL;
		
		aux->next=nodeDec;
		aux = nodeDec;

		listIds=listIds->next;	
	}
	return idl;
}

is_declaration_list* insertDeclarations(is_declaration_list* node, is_declaration_list* head){			
	if(head==NULL){
		return node;}
	is_declaration_list* aux=node;
	while(aux->next!=NULL)
		aux=aux->next;
	aux->next = head;
	return node;
} 

is_id_list* insertIds(char *id, is_id_list* head,int linha,int coluna){
	is_id_list* node = (is_id_list*)malloc(sizeof(is_id_list));
	node->id=(char*)strdup(id);
	node->linha=linha;
	node->coluna=coluna;
	node->next=NULL;	
	if(head==NULL){
		return node;	
	}
	node->next = head;
	return node;
}

is_func_params* insertParams(char* id,char* tipo, is_func_params* head,int linha,int coluna){
	is_func_params* node = (is_func_params*)malloc(sizeof(is_func_params));
	node->id = (char*)strdup(id);
	node->tipo = tipo;
	node->linha = linha;
	node->coluna = coluna;
	node->next = NULL;
	if(head==NULL)
		return node;
	node->next = head;
	return node;	
}

is_func_header* createNodeHeader(char* id, char* tipo, is_func_params* params,int linha,int coluna){
	is_func_header* node = (is_func_header*)malloc(sizeof(is_func_header));
	node->id = (char*)strdup(id);
	node->linha=linha;
	node->coluna=coluna;
	if(tipo!=NULL)
		node->tipo =tipo;
	else
		node->tipo=NULL;
	node->parametros = params;	
	return node;
}

is_declaration_list* createNodeFuncDec(is_func_header* header, is_func_body* body){
	is_declaration_list* node = (is_declaration_list*)malloc(sizeof(is_declaration_list));	
	node->valF = (is_funcdec*)malloc(sizeof(is_funcdec));
	node->valF->header = header;
	node->valF->body = body;
	node->valV = NULL;
	node->next=NULL;
	return node;
}

is_func_body* insertBodyState(is_statement* statement,is_func_body* head){
	if(statement==NULL)
		return head;
	is_func_body* aux=head;
	is_func_body* node = (is_func_body*)malloc(sizeof(is_func_body));
	node->valV = NULL;	
	node->valS = statement;
	node->next=NULL;
	if(head==NULL){
		head=node;		
	}else{
		while(aux->next!=NULL){
			aux=aux->next;			
		}
		aux->next = node;
	}
	return head;	
} 

is_func_body* insertBodyVar(is_declaration_list* idl, is_func_body* head){
	is_func_body* aux=head;
	while(idl!=NULL){
		is_func_body* node = (is_func_body*)malloc(sizeof(is_func_body));
		node->valV = idl->valV;	
		node->valS = NULL;
		node->next=NULL;
		if(head==NULL){
			head=node;
			aux=node;		
		}else{
			while(aux->next!=NULL){
				aux=aux->next;			
			}
		aux->next = node;
		}
		idl = idl->next;
	}
	return head;	
} 

/*is_exp_list* insertExpList(is_exp_list* exp1, is_exp_list* exp2, char* op, is_exp_list* head){
	//printf("Exp1: %s Exp2: %s\n", exp1->num1, exp2->num1)
	is_exp_list* node = (is_exp_list*)malloc(sizeof(is_exp_list));
	if(strcmp(exp1->op,"NUMBER")==0 && strcmp(exp2->op,"NUMBER")==0){
		node->num1 = (char*)strdup(exp1->num1);
		node->num2 = (char*)strdup(exp2->num1);
		node->op = (char*)strdup(op);
		node->next = NULL;
	}else if(!(strcmp(exp1->op,"NUMBER")==0) && strcmp(exp2->op,"NUMBER")==0){
		node->next = exp1;
		node->num1 = NULL;
		node->num2 = (char*)strdup(exp2->num1);
		node->op = (char*)strdup(op);
		return node;
	}else if(strcmp(exp1->op,"NUMBER")==0 && !(strcmp(exp2->op,"NUMBER")==0)){
		node->next = exp2;
		node->num1 = (char*)strdup(exp1->num1);
		node->num2 = NULL;
		node->op = (char*)strdup(op);
		return node;	
	}else{
		printf("ENTREIIII");	
	}
	if(head==NULL){
		return node;
	}
	node->next=head;
	return node;
		
	
}*/

is_nodeExp* insertNodeTree(is_nodeExp* exp1,is_nodeExp* exp2,char* operation,int linha,int coluna){
	is_nodeExp* node=(is_nodeExp*) malloc(sizeof(is_nodeExp));
	node->num = NULL;
	node->operation=(char*) malloc(sizeof(char)*10);
	strcpy(node->operation,operation);
	node->left=exp1;
	node->right=exp2;
	node->linha=linha;
	node->coluna=coluna;	
	return node;

}

is_number* createNodeNumber(char* num, char* tipo){
	is_number* node =(is_number*)malloc(sizeof(is_number));
	node->numero = (char*)strdup(num);
	node->tipo = (char*)strdup(tipo);
	return node;
}

is_nodeExp* createNodeTree(is_number* num,int linha,int coluna){
	is_nodeExp* node = (is_nodeExp*)malloc(sizeof(is_nodeExp));
	node->num = num;
	node->left = NULL;
	node->right = NULL;
	node->operation=NULL;
	node->linha=linha;
	node->coluna=coluna;
	return node;
}



//Function Call
is_nodeExp* insertSpecialNode(is_nodeExp* head,is_nodeExp* exp){
	is_nodeExp* newNode = (is_nodeExp*)malloc(sizeof(is_nodeExp));
	newNode->operation=(char *) malloc(sizeof(char)*5);
	strcpy(newNode->operation,"next");
	newNode->right=exp;
	newNode->left=head;
	return newNode;
}

is_nodeExp* createCallNode(is_nodeExp* exp1,char* id,int linha, int coluna){
	is_nodeExp* node=(is_nodeExp*) malloc(sizeof(is_nodeExp));
	is_number* number=(is_number*) malloc(sizeof(is_number));
	number->numero=(char*) strdup(id);
	node->num=number;
	node->operation=(char* )malloc(sizeof(char)*30);
	strcpy(node->operation,"Call");
	node->left=exp1;
	node->right=NULL;
	node->linha=linha;
	node->coluna=coluna;
	return node;
}

void printPontos(int numeroPontos){
	int i;	
	for(i=0;i<numeroPontos;i++){
		printf(".");
	}
}


void printExp(is_nodeExp* head,int numeroPontos,int anotada){
	is_nodeExp* aux= head;
	if(aux->operation!=NULL){
		if(strcmp(aux->operation,"next")==0){
			printExp(aux->right,numeroPontos,anotada);
			if(aux->left!=NULL){
				printExp(aux->left,numeroPontos,anotada);
			}
			return;
		}
		printPontos(numeroPontos);
		if(anotada==1 && aux->type!=none)
			printf("%s - %s\n",aux->operation,name_types[aux->type]);
		else
			printf("%s\n",aux->operation);
		if(strcmp(aux->operation,"Call")==0){
			printPontos(numeroPontos+2);
			if(anotada==1){
				printf("Id(%s) - (",aux->num->numero);
				is_nodeExp* param = aux->left;
				while(param!=NULL){
					if(param->left==NULL)
						printf("%s",name_types[param->right->type]);
					else
						printf("%s,",name_types[param->right->type]);
					param = param->left;
				}
				printf(")\n");
			}
			else
				printf("Id(%s)\n",aux->num->numero);
		}
	
	}
	else if(aux->operation==NULL){
		printPontos(numeroPontos);
		if(anotada==1)
			printf("%s(%s) - %s\n",aux->num->tipo,aux->num->numero,name_types[aux->type]);
		else
			printf("%s(%s)\n",aux->num->tipo,aux->num->numero);
	}
	if(aux->left!=NULL){
		printExp(aux->left,numeroPontos+2,anotada);	

	}
	if(aux->right!=NULL){
		printExp(aux->right,numeroPontos+2,anotada);	
	}
}






void printParam(is_func_params* params, int numeroPontos){
	printPontos(numeroPontos);
	printf("ParamDecl\n");
	printPontos(numeroPontos+2);
	printf("%s\n",params->tipo);
	printPontos(numeroPontos+2);
	printf("Id(%s)\n",params->id);
	if(params->next!=NULL){
		printParam(params->next,numeroPontos);
	}

}

void printHeader(is_func_header *head,int numeroPontos){
	printPontos(numeroPontos);
	printf("FuncHeader\n");
	numeroPontos+=2;
	printPontos(numeroPontos);
	printf("Id(%s)\n",head->id);
	if(head->tipo!=NULL){
		printPontos(numeroPontos);
		printf("%s\n",head->tipo);
	}
	printPontos(numeroPontos);
	printf("FuncParams\n");
	if(head->parametros != NULL){
		printParam(head->parametros,numeroPontos+2);
	}
		
}

void printBody(is_func_body* body, int numeroPontos,int anotada){
	if(body->valV!=NULL){
		printVarDeclaration(body->valV,numeroPontos);
	}else if(body->valS!=NULL){
		printStatement(body->valS,numeroPontos,anotada);
	}	
	if(body->next!=NULL)
		printBody(body->next,numeroPontos,anotada);
}

void printStateList(is_statelist* statelist,int numeroPontos,int anotada){
	printStatement(statelist->statement,numeroPontos,anotada);
	if(statelist->next!=NULL)
		printStateList(statelist->next,numeroPontos,anotada);

}


void printStatement(is_statement* statement, int numeroPontos,int anotada){
	if(strcmp(statement->identificador,"Assign")==0){
		printPontos(numeroPontos);
		if(anotada==1){
			printf("Assign - %s\n",name_types[statement->node->type]);
			printPontos(numeroPontos+2);
			printf("Id(%s) - %s\n",statement->id,name_types[statement->node->type]);
		}
		else{
			printf("Assign\n");
			printPontos(numeroPontos+2);
			printf("Id(%s)\n",statement->id);
		}
		printExp(statement->node,numeroPontos+2,anotada);
	}else if(strcmp(statement->identificador,"funcinvocation")==0){
		printExp(statement->node,numeroPontos,anotada);
	}else if(strcmp(statement->identificador,"ParseArgs")==0){
		printPontos(numeroPontos);
		if(anotada==1){
			printf("ParseArgs - %s\n",name_types[statement->node->type]);
			printPontos(numeroPontos+2);
			printf("Id(%s) - %s\n",statement->id,name_types[statement->node->type]);
		}
		else{
			printf("ParseArgs\n");
			printPontos(numeroPontos+2);
			printf("Id(%s)\n",statement->id);
		}
		printExp(statement->node,numeroPontos+2,anotada);		
	}else if(strcmp(statement->identificador,"PrintExp")==0){
		printPontos(numeroPontos);
		printf("Print\n");
		printExp(statement->node,numeroPontos+2,anotada);
	}else if(strcmp(statement->identificador,"Print")==0){
		printPontos(numeroPontos);
		printf("Print\n");
		printPontos(numeroPontos+2);
		printf("StrLit(%s)\n",statement->id);
	}else if(strcmp(statement->identificador,"Return")==0){
		printPontos(numeroPontos);
		printf("Return\n");
		if(statement->node != NULL)
			printExp(statement->node,numeroPontos+2,anotada);
	}else if(strcmp(statement->identificador,"Block")==0){
		printPontos(numeroPontos);
		printf("Block\n");
		if(statement->next!=NULL)
			printStateList(statement->next,numeroPontos+2,anotada);		
	}else if(strcmp(statement->identificador,"For")==0){
		printPontos(numeroPontos);
		printf("For\n");
		if(statement->node != NULL)
			printExp(statement->node,numeroPontos+2,anotada);
		printPontos(numeroPontos+2);
		printf("Block\n");
		if(statement->next!=NULL)
			printStateList(statement->next,numeroPontos+4,anotada);	
	}else if(strcmp(statement->identificador,"If")==0){
		printPontos(numeroPontos);
		printf("If\n");
		printExp(statement->node,numeroPontos+2,anotada);
		printPontos(numeroPontos+2);
		printf("Block\n");
		if(statement->next!=NULL)
			printStateList(statement->next,numeroPontos+4,anotada);
		printPontos(numeroPontos+2);
		printf("Block\n");
		if(statement->elseNext!=NULL){
			printStateList(statement->elseNext,numeroPontos+4,anotada);		
		}	
	}
}

void printFuncDeclaration(is_funcdec * funcdec,int numeroPontos,int anotada){
	printPontos(numeroPontos);
	printf("FuncDecl\n");
	numeroPontos+=2;
	printHeader(funcdec->header,numeroPontos);
	printPontos(numeroPontos);
	printf("FuncBody\n");
	numeroPontos+=2;
	if( funcdec->body !=NULL){
		printBody(funcdec->body,numeroPontos,anotada);
	}
}

void printVarDeclaration(is_vardec* vardec,int numeroPontos){
	printPontos(numeroPontos);
	printf("VarDecl\n");
	numeroPontos+=2;
	printPontos(numeroPontos);
	printf("%s\n",vardec->tipo);
	printPontos(numeroPontos);
	printf("Id(%s)\n",vardec->id);
}

int tamanhoBloco(is_statelist* lista){
	is_statelist* aux=lista;
	int conta=0;
	while(aux!=NULL){
		conta++;
		aux=aux->next;
	}
	return conta;
}
is_statement* createAssignState(char* identificador, is_nodeExp* node,is_statelist* next,is_statelist* elseNext,char* id,int linhaId,int colunaId,int linhaAssign,int colunaAssign){
	is_statement* newNode = (is_statement*)malloc(sizeof(is_statement));
	newNode->identificador =(char*)strdup(identificador);
	newNode->node = node;
	newNode->next = next;
	newNode->elseNext = elseNext;
	newNode->linhaId=linhaId;
	newNode->colunaId=colunaId;
	newNode->linhaAssign=linhaAssign;
	newNode->colunaAssign=colunaAssign;
	if(id!=NULL){
		newNode->id = (char*)strdup(id);
	}
	else{
		newNode->id=NULL;
	}
	return newNode;
}

is_statement* createStatement(char* identificador, is_nodeExp* node,is_statelist* next,is_statelist* elseNext,char* id){
	is_statement* newNode = (is_statement*)malloc(sizeof(is_statement));
	newNode->identificador =(char*)strdup(identificador);
	newNode->node = node;
	newNode->next = next;
	newNode->elseNext = elseNext;
	if(id!=NULL){
		newNode->id = (char*)strdup(id);
	}
	else{
		newNode->id=NULL;
	}
	return newNode;
}

is_statement* createStatementReturn(char* identificador, is_nodeExp* node,is_statelist* next,is_statelist* elseNext,char* id,int linha, int coluna){
	is_statement* newNode = (is_statement*)malloc(sizeof(is_statement));
	newNode->identificador =(char*)strdup(identificador);
	newNode->node = node;
	newNode->next = next;
	newNode->elseNext = elseNext;
	if(id!=NULL){
		newNode->id = (char*)strdup(id);
	}
	else{
		newNode->id=NULL;
	}
	if(node==NULL){
		newNode->linhaId=linha;
		newNode->colunaId=coluna+6;
	}else{
		newNode->linhaId=node->linha;
		newNode->colunaId=node->coluna;	
	}
	return newNode;
}

is_statelist* insertStatement(is_statement* novoState,is_statelist* head){
	if(novoState!=NULL){
		is_statelist* node =(is_statelist *) malloc(sizeof(is_statelist));
		node->statement=novoState;
		node->next=head;	
		return node;
	}
	return head;
}

//Liberta Memoria
void freeVarDeclaration(is_vardec* vardec){
	free(vardec->id);
	free(vardec->tipo);
}

void freeFuncDeclaration(is_funcdec * funcdec){
	freeHeader(funcdec->header);
	free(funcdec->header);
	if( funcdec->body !=NULL){
		freeBody(funcdec->body);
	}
}
void freeParam(is_func_params* params){
	free(params->id);
	if(params->next!=NULL){
		freeParam(params->next);
	}
	free(params);

}

void freeHeader(is_func_header *head){
	free(head->id);
	if(head->tipo!=NULL){
		free(head->tipo);
	}
	if(head->parametros != NULL){
		freeParam(head->parametros);
	}		
}
void freeBody(is_func_body* body){
	if(body->valV!=NULL){
		freeVarDeclaration(body->valV);
	}else if(body->valS!=NULL){
		freeStatement(body->valS);
	}	
	if(body->next!=NULL){
		freeBody(body->next);
	}
	free(body);
}


void freeStateList(is_statelist* statelist){
	if(statelist->statement!=NULL)
		free(statelist->statement);
	if(statelist->next!=NULL)
		freeStateList(statelist->next);	
	free(statelist);
}


void freeStatement(is_statement* statement){
	if(strcmp(statement->identificador,"Assign")==0){
		free(statement->identificador);
		if(statement->id!=NULL)
			free(statement->id);
		if(statement->node!=NULL)
			freeExp(statement->node);
	}else if(strcmp(statement->identificador,"funcinvocation")==0){
		free(statement->identificador);
		if(statement->node!=NULL)
			freeExp(statement->node);
	}else if(strcmp(statement->identificador,"ParseArgs")==0){
		free(statement->identificador);
		if(statement->id!=NULL)
			free(statement->id);
		if(statement->node!=NULL)
			freeExp(statement->node);		
	}else if(strcmp(statement->identificador,"PrintExp")==0){
		free(statement->identificador);
		if(statement->node!=NULL)
			freeExp(statement->node);
	}else if(strcmp(statement->identificador,"Print")==0){
		free(statement->identificador);
		if(statement->id!=NULL)
			free(statement->id);
	}else if(strcmp(statement->identificador,"Return")==0){
		free(statement->identificador);
		if(statement->node!=NULL)
			freeExp(statement->node);
	}else if(strcmp(statement->identificador,"Block")==0){
		free(statement->identificador);
		if(statement->next!=NULL)
			freeStateList(statement->next);		
	}else if(strcmp(statement->identificador,"For")==0){
		free(statement->identificador);
		if(statement->node != NULL)
			freeExp(statement->node);
		if(statement->next!=NULL)
			freeStateList(statement->next);	
	}else if(strcmp(statement->identificador,"If")==0){
		free(statement->identificador);
		freeExp(statement->node);
		if(statement->next!=NULL)
			freeStateList(statement->next);
		if(statement->elseNext!=NULL){
			freeStateList(statement->elseNext);		
		}	
	}
}


void freeExp(is_nodeExp* head){
	is_nodeExp* aux= head;
	if(aux->operation!=NULL){
		free(aux->operation);
	}
	if(aux->num!=NULL){
		if(aux->num->tipo!=NULL)
			free(aux->num->tipo);
		if(aux->num->numero!=NULL)
			free(aux->num->numero);
	}
	if(aux->left!=NULL)
		freeExp(aux->left);	

	if(aux->right!=NULL)
		freeExp(aux->right);
	free(head);	
}


