%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ast.h"
#include "y.tab.h"
int yydebug=1;
int yylex (void);
void yyerror(char* s);

int vazia=1;

is_program* myprogram;
is_declaration_list* headDeclarations = NULL;
is_id_list* headIds = NULL;
is_func_params* headParams = NULL;
is_func_body* headBody= NULL;

is_nodeExp* headCall=NULL;

%}

%union{
	int value;
	char* id;
	double dvalue;
	is_program* ip;
	is_declaration_list* idl;
	is_vardec* iv;
	is_id_list* listIds;
	is_func_params* ifp;
	is_func_body* ifb;
	is_number* number;
	is_nodeExp* nodeExp;
	is_statement* is;
	is_statelist* stateList;
}

%token TRICK
%token <id> INTLIT
%token <id> STRLIT
%token <id> ID
%token <id> REALLIT
%type <nodeExp> expr
%type <number> number
%type <nodeExp> funcinvocation
%type <nodeExp> funcinvOpt
%type <nodeExp> commaexpOpt
%type <nodeExp> statementexprOpt
%type <is> statement
%type <stateList> statementelseOpt
%type <stateList> statementOpt
%type <is> parseargs

%token SEMICOLON RETURN RPAR RSQ RBRACE BLANKID PACKAGE AND ASSIGN STAR COMMA DIV EQ GE GT LBRACE LE LPAR LSQ LT MINUS MOD NE NOT OR PLUS ELSE FOR IF VAR INT FLOAT32 BOOL STRING PRINT PARSEINT FUNC CMDARGS RESERVED 

%right ASSIGN
%left OR
%left AND
%left LT GT EQ NE LE GE
%left PLUS MINUS
%left STAR DIV MOD
%left TRICK
%left NOT


%type <ip> program;
%type <idl> declarations;
%type <id> type;
%type <idl> varspec;
%type <idl> vardeclaration;
%type <listIds> varspecOpt;
%type <ifp> parametersOpt;
%type <ifp> parameters;
%type <ifp> funcdeclarationparOpt;
%type <id> funcdeclarationtypeOpt;
%type <idl> funcdeclaration;
%type <ifb> funcbody;
%type <ifb> varsandstatements;
%type <ifb> varsOpt

%%

program: PACKAGE ID SEMICOLON declarations {$$=myprogram=insert_program($4);}
	|/*EMPTY*/ {$$=myprogram=NULL;}
	;

declarations: /*EMPTY*/ {$$=NULL;}
            | vardeclaration SEMICOLON declarations {headDeclarations=$$=insertDeclarations($1,headDeclarations);}
            | funcdeclaration SEMICOLON declarations{headDeclarations=$$=insertDeclarations($1,headDeclarations);}
            ;

vardeclaration: VAR varspec {$$=$2;}
              | VAR LPAR varspec SEMICOLON RPAR {$$=$3;}
              ;

varspec: ID varspecOpt type {$$=createNodeVar($1,$3,$2,@1.first_line,@1.first_column);headIds=NULL;}
;

varspecOpt: /*EMPTY*/ {$$=NULL;}
          | COMMA ID varspecOpt {headIds=$$=insertIds($2,headIds,@2.first_line,@2.first_column);}
	  ; 

type: INT {$$=strdup("Int");}
    | FLOAT32 {$$=strdup("Float32");}
    | BOOL {$$=strdup("Bool");}
    | STRING {$$=strdup("String");}
    ;

funcdeclaration: FUNC ID LPAR funcdeclarationparOpt RPAR funcdeclarationtypeOpt funcbody { $$=createNodeFuncDec(createNodeHeader($2,$6,$4,@2.first_line,@2.first_column),$7); }// receber a FUNCBODY
		;

funcdeclarationparOpt: /*EMPTY*/ {$$=NULL;}
                     | parameters {$$=$1;}
                     ;
funcdeclarationtypeOpt:/*EMPTY*/ {$$=NULL;}
                      | type {$$=$1;}
 		      ;

parameters: ID type parametersOpt {$$=insertParams($1,$2,headParams,@1.first_line,@1.first_column);headParams=NULL;}
	  ;

parametersOpt: /*EMPTY*/ {$$=NULL;}
             | COMMA ID type parametersOpt {headParams=$$=insertParams($2,$3,headParams,@2.first_line,@2.first_column);}
	     ;

funcbody: LBRACE varsandstatements RBRACE {$$=$2;headBody=NULL;}
	; 

varsandstatements:/*EMPTY*/ {$$=NULL;}
                 | varsandstatements varsOpt SEMICOLON {$$=$2;}  //inserir
		 ;
varsOpt:/*EMPTY*/ {$$=NULL;}                                                
       | statement {headBody=$$=insertBodyState($1,headBody);}//printTest($1);}
       | vardeclaration {headBody=$$=insertBodyVar($1,headBody);} //Criar
       ;

statement: ID ASSIGN expr {$$=createAssignState("Assign",$3,NULL,NULL,$1,@1.first_line,@1.first_column,@2.first_line,@2.first_column);} //printar ID nodeExp
         | LBRACE statementOpt RBRACE {if(tamanhoBloco($2)>=2)$$=createStatement("Block",NULL,$2,NULL,NULL);else if(tamanhoBloco($2)==1)$$=$2->statement;else $$=NULL;}// Block
         | funcinvocation {$$=createStatement("funcinvocation",$1,NULL,NULL,NULL);}
         | parseargs {$$=$1;}
         | PRINT LPAR expr RPAR {$$=createStatement("PrintExp",$3,NULL,NULL,NULL);}
         | PRINT LPAR STRLIT RPAR {$$=createStatement("Print",NULL,NULL,NULL,$3);}
         | RETURN statementexprOpt {$$=createStatementReturn("Return",$2,NULL,NULL,NULL,@1.first_line,@1.first_column);}
         | FOR statementexprOpt LBRACE statementOpt RBRACE {$$=createStatement("For",$2,$4,NULL,NULL);}
         | IF expr LBRACE statementOpt RBRACE statementelseOpt {$$=createStatement("If",$2,$4,$6,NULL);}
         | error {$$=NULL;}
         ;

statementelseOpt: /*EMPTY*/ {$$=NULL;}
                | ELSE LBRACE statementOpt RBRACE {$$=$3;}
		; 

statementexprOpt: /*EMPTY*/ {$$=NULL;}
                | expr {$$=$1;}
		;

statementOpt: /*EMPTY*/ {$$=NULL;}
            | statement SEMICOLON statementOpt {$$=insertStatement($1,$3);}
	    ;

parseargs: ID COMMA BLANKID ASSIGN PARSEINT LPAR CMDARGS LSQ expr RSQ RPAR {$$=createAssignState("ParseArgs",$9,NULL,NULL,$1,@1.first_line,@1.first_column,@5.first_line,@5.first_column);}
         | ID COMMA BLANKID ASSIGN PARSEINT LPAR CMDARGS LSQ error RSQ RPAR{$$=NULL;}
	 ;

funcinvocation: ID LPAR funcinvOpt RPAR {$$=createCallNode($3,$1,@1.first_line,@1.first_column);} //COLOCAR O ID
              | ID LPAR error RPAR {$$=NULL;}
	      ;

funcinvOpt: /*EMPTY*/ {$$=NULL;}
          | expr commaexpOpt {$$=insertSpecialNode($2,$1);}
	  ;

commaexpOpt: /*EMPTY*/	{$$=NULL;}
           | COMMA expr commaexpOpt {$$=insertSpecialNode($3,$2);}
           ;


expr: expr PLUS expr {$$=insertNodeTree($1,$3,"Add",@2.first_line,@2.first_column);}
    | expr MINUS expr {$$=insertNodeTree($1,$3,"Sub",@2.first_line,@2.first_column);} 
    | expr STAR expr {$$=insertNodeTree($1,$3,"Mul",@2.first_line,@2.first_column);}
    | expr DIV expr {$$=insertNodeTree($1,$3,"Div",@2.first_line,@2.first_column);}
    | expr MOD expr {$$=insertNodeTree($1,$3,"Mod",@2.first_line,@2.first_column);}
    | LPAR expr RPAR {$$=$2;}
    | LPAR error RPAR {$$=NULL;}
    | expr OR expr  {$$=insertNodeTree($1,$3,"Or",@2.first_line,@2.first_column);}
    | expr AND expr {$$=insertNodeTree($1,$3,"And",@2.first_line,@2.first_column);}
    | expr LT expr {$$=insertNodeTree($1,$3,"Lt",@2.first_line,@2.first_column);}
    | expr GT expr {$$=insertNodeTree($1,$3,"Gt",@2.first_line,@2.first_column);}
    | expr EQ expr {$$=insertNodeTree($1,$3,"Eq",@2.first_line,@2.first_column);}
    | expr NE expr {$$=insertNodeTree($1,$3,"Ne",@2.first_line,@2.first_column);}
    | expr LE expr {$$=insertNodeTree($1,$3,"Le",@2.first_line,@2.first_column);}
    | expr GE expr {$$=insertNodeTree($1,$3,"Ge",@2.first_line,@2.first_column);}
    | NOT expr {$$=insertNodeTree($2,NULL,"Not",@1.first_line,@1.first_column);}
    | PLUS expr %prec TRICK {$$=insertNodeTree($2,NULL,"Plus",@1.first_line,@1.first_column);}
    | MINUS expr %prec TRICK {$$=insertNodeTree($2,NULL,"Minus",@1.first_line,@1.first_column);}
    | number {$$=createNodeTree($1,@1.first_line,@1.first_column);} 
    | funcinvocation {$$=$1;}
;

number: INTLIT {$$=createNodeNumber($1,"IntLit");}
      | REALLIT {$$=createNodeNumber($1,"RealLit");}
      | ID {$$=createNodeNumber($1,"Id");}
      ;

%%
/*void yyerror(char *msg) {
    printf("%s coluna %d", msg, yylval.value);
}*/
