#include "structures.h"
#include "semantics.h"
#include "symbol_table.h"
#include "generatecode.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const char* name_types[] = {"int","string","float32","none","undef","bool"};

//go through AST, call insert table,etc
basic_type getType(char *str){
	if(str==NULL)
		return none;
	if(strcmp(str,"Int")==0){
		return integer;
	}else if(strcmp(str,"IntLit")==0){
		return integer;	
	}else if(strcmp(str,"RealLit")==0){
		return float32;	
	}else if(strcmp(str,"Float32")==0){
		return float32;	
	}else if(strcmp(str,"Bool")==0){
		return boolean;	
	}else if(strcmp(str,"String")==0){
		return string;	
	}
	else{
		return none;	
	}
}

int check_program(is_program* myprogram,int printar){
	int erros=0;
	table* global_table = (table*)malloc(sizeof(table));
	global_table->nextFunction = NULL;
	global_table->nextDecl = NULL;
	global_table->previous = NULL;
	is_declaration_list* aux = myprogram->dlist;
	while(aux!=NULL){
		if(aux->valF!=NULL){
			//check funcDeclaration
			erros+=check_funcDeclaration(global_table,aux->valF);		
		}
		else if(aux->valV!=NULL){
			//check varDeclaration
			erros+=check_varDeclaration(global_table,aux->valV);
		}
		aux = aux->next;
	}
	aux = myprogram->dlist;
	table* tabela = global_table->nextFunction;
	while(tabela!=NULL && aux!=NULL){
		if(aux->valF!=NULL && strcmp(tabela->name,aux->valF->header->id)==0){
			erros+=check_funcBody(global_table,tabela,aux->valF);
			tabela = tabela->nextFunction;
		}
		aux = aux->next;
	}
	tabela = global_table->nextFunction;
	while(tabela!=NULL){
		table_element* aux = tabela->nextDecl;
		while(aux!=NULL){
			if(aux->isVarDec==1 && aux->used==0){
				printf("Line %d, column %d: Symbol %s declared but never used\n",aux->linha,aux->coluna,aux->name);				
				erros+=1;			
			}
			aux = aux->next;
		}
		tabela = tabela->nextFunction;
	}
	if(erros==0){
		if(printar==1)
			printTables(global_table);
		else
			generate_program(myprogram,global_table);
	}
	return erros;
}


int check_varDeclaration(table* tab,is_vardec* var){
	//verificar se var ja esta declarada...
	table_element* aux = tab->nextDecl;
	int state=0;
	while(aux!=NULL){
		if(strcmp(aux->name,var->id)==0)
			state=1;
		aux = aux->next;	
	}
	if(state==0)
		insertVarDec(tab,var->id,getType(var->tipo),var->linha,var->coluna);
	else
		//error msg Symbol already defined
		printf("Line %d, column %d: Symbol %s already defined\n",var->linha,var->coluna,var->id);
	return state;
}

int check_varDeclarationFunc(table* tabela, is_vardec* var){
	int state=0;
	table_element* aux = tabela->nextDecl;
	if(aux==NULL){ //ainda não tenho nada na tabela
		paramList* parametro = tabela->listaParam;
		while(parametro!=NULL){
			if(strcmp(parametro->id,var->id)==0){
				state=1;
				break;
			}
			parametro=parametro->next;		
		}
	}else{
		paramList* parametro = tabela->listaParam;
		while(parametro!=NULL){
			if(strcmp(parametro->id,var->id)==0){
				state=1;
				break;
			}
			parametro=parametro->next;		
		}
		int open_close=0;
		while(state==0 && aux!=NULL){ 
			open_close+=aux->open_close;
			if(open_close<=0){
				if(aux->isVarDec==1 && strcmp(aux->name,var->id)==0){
					state = 1;				
				}			
			}			
			aux = aux->next; //chegar ao fim da lista	
		}
						
	}
	if(state==0)
		insertVarDec(tabela,var->id,getType(var->tipo),var->linha,var->coluna);
	else //erro already defined
		printf("Line %d, column %d: Symbol %s already defined\n",var->linha,var->coluna,var->id);
	return state;
}


int check_funcDeclaration(table* tab,is_funcdec* funcdec){
	int erros=0;
	int state = 0;
	int auxErro=0;
	table_element* aux2 = tab->nextDecl;
	while(aux2!=NULL){
		if(strcmp(aux2->name,funcdec->header->id)==0)
			state = 1;
		aux2 = aux2->next;	
	}	
	if(state==0){  //tabela ainda não existe
	//Cria Lista Params
		is_func_params* aux = funcdec->header->parametros;
		paramList* parametros=NULL;
		paramList* node = parametros;
		while(aux!=NULL){
			paramList* head = parametros;
			while(head!=NULL){
				if(strcmp(head->id,aux->id)==0){
					printf("Line %d, column %d: Symbol %s already defined\n",aux->linha,aux->coluna,aux->id);
					erros+=1;
					auxErro=1;
					break;
				}
				head = head->next;
			}				
			if(auxErro==0){						
				paramList* novoParam=(paramList*)malloc(sizeof(paramList));
				novoParam->id=strdup(aux->id);
				novoParam->type=getType(aux->tipo);
				novoParam->next=NULL;
				if(parametros==NULL){
					parametros = novoParam;
					node = parametros;
				}else{
					node->next=novoParam;
					node=novoParam;
				}
			}
			auxErro=0;
			aux = aux->next;			
		}
		

		//criar uma nova tabela e inserir a declaracao na tabela global
		if(auxErro==0){
			insertFuncDec(tab,funcdec->header->id,getType(funcdec->header->tipo),parametros);
		}
		return erros;
	}
	else{           
		printf("Line %d, column %d: Symbol %s already defined\n",funcdec->header->linha,funcdec->header->coluna,funcdec->header->id);	 
	}
	return state;
}

int check_funcBody(table* global,table* tabela, is_funcdec* funcdec){
	int erros=0;
	is_func_body* funcBody=funcdec->body;
	while(funcBody!=NULL){
		if(funcBody->valV!=NULL){
			erros+=check_varDeclarationFunc(tabela,funcBody->valV);
		}
		if(funcBody->valS!=NULL){
			erros+=check_statement(global,tabela,funcBody->valS);
		}
		funcBody=funcBody->next;
	}
	return erros;
}

int check_parseargs(table* global, table* tabela, is_statement* statement){
	int erros=0;
	table_element* res = check_exists_var(tabela,statement->id);
	if(res==NULL){ //cannot find
		printf("Line %d, column %d: Cannot find symbol %s\n",statement->linhaId,statement->colunaId,statement->id);
		erros+=1;
	}
	basic_type tipo = check_expression(global,tabela,statement->node,&erros);
	if(res==NULL){
		printf("Line %d, column %d: Operator strconv.Atoi cannot be applied to types undef, %s\n",statement->linhaAssign,statement->colunaAssign,name_types[tipo]);
		erros+=1;	
	}else if(res->type!=integer || tipo!=integer){
		printf("Line %d, column %d: Operator strconv.Atoi cannot be applied to types %s, %s\n",statement->linhaAssign,statement->colunaAssign,name_types[res->type],name_types[tipo]);
		erros+=1;
	}/*else if(res!=NULL && tipo!=integer){
		printf("Line %d, column %d: Operator strconv.Atoi cannot be applied to types %s, %s\n",statement->linhaAssign,statement->colunaAssign,name_types[tipo],name_types[res->type]);
		erros+=1;
	}*/
	return erros;
}

int check_statement(table* global, table* tabela, is_statement* statement){
	int erros=0;	
	if(strcmp(statement->identificador,"Assign")==0){
		erros+=check_assign(global,tabela,statement);	
	}else if(strcmp(statement->identificador,"funcinvocation")==0){
		check_expression(global,tabela,statement->node,&erros);
	}else if(strcmp(statement->identificador,"ParseArgs")==0){
		erros+=check_parseargs(global,tabela,statement);
	}else if(strcmp(statement->identificador,"PrintExp")==0){
		check_expression(global,tabela,statement->node,&erros);	
	}else if(strcmp(statement->identificador,"Print")==0){
		//Anotar sim ou nao ?
	}else if(strcmp(statement->identificador,"Return")==0){
		basic_type res = none;
		if(statement->node!=NULL)
			res = check_expression(global,tabela,statement->node,&erros);
		if(res!=tabela->returnType){
			erros+=1;
			printf("Line %d, column %d: Incompatible type %s in return statement\n",statement->linhaId,statement->colunaId,name_types[res]);		
		}	
	}else if(strcmp(statement->identificador,"Block")==0){
		insertOpenClose(tabela,1);
		if(statement->next!=NULL)
			erros+=check_stateList(global, tabela, statement->next);
		insertOpenClose(tabela,-1);	
	}else if(strcmp(statement->identificador,"For")==0){
		if(statement->node!=NULL){
			basic_type res = check_expression(global,tabela,statement->node,&erros);
			if(res!=boolean){
				printf("Line %d, column %d: Incompatible type %s in for statement\n",statement->node->linha,statement->node->coluna,name_types[res]);
			erros+=1;			
			}
		}
		insertOpenClose(tabela,1);
		if(statement->next!=NULL)
			erros+=check_stateList(global, tabela, statement->next);
		insertOpenClose(tabela,-1);
	}else if(strcmp(statement->identificador,"If")==0){
		basic_type res = check_expression(global,tabela,statement->node,&erros);
		if(res!=boolean){
			printf("Line %d, column %d: Incompatible type %s in if statement\n",statement->node->linha,statement->node->coluna,name_types[res]);
			erros+=1;			
		}
		insertOpenClose(tabela,1);
		if(statement->next!=NULL)
			erros+=check_stateList(global, tabela, statement->next);
		insertOpenClose(tabela,-1);
		if(statement->elseNext!=NULL){
			insertOpenClose(tabela,1);
			erros+=check_stateList(global, tabela, statement->elseNext);
			insertOpenClose(tabela,-1);
		}
	}
	return erros;	
}

int check_stateList(table* global, table* tabela, is_statelist* statelist){
	int erros=0;
	erros+=check_statement(global,tabela,statelist->statement);
	if(statelist->next!=NULL)
		erros+=check_stateList(global,tabela,statelist->next);
	return erros;
}

table_element* check_exists_var(table* tabela, char *id){
	table_element* aux = tabela->nextDecl;
	if(aux==NULL){
		paramList* parametro = tabela->listaParam;
		while(parametro!=NULL){
			if(strcmp(parametro->id,id)==0){
				table_element* new_element = (table_element*)malloc(sizeof(table_element));
				new_element->type=parametro->type;
				return new_element;
			}
			parametro=parametro->next;		
		}
		//procurar na global
		aux = tabela->previous;
		while(aux!=NULL){
			if(aux->isVarDec==1 && strcmp(aux->name,id)==0){
				aux->used = 1;
				return aux;
			}
			aux = aux->previous;
		}	
	}else{
		while(aux->next!=NULL) //andar até ao fim
			aux=aux->next;	
		int open_close = 0;		
		while(aux!=NULL){
			open_close+=aux->open_close;
			if(open_close>=0){
				if(aux->isVarDec==1 && strcmp(aux->name,id)==0){
					aux->used = 1;
					return aux;
				}			
			}
			aux = aux-> previous;		
		}
		paramList* parametro = tabela->listaParam;
		while(parametro!=NULL){
			if(strcmp(parametro->id,id)==0){
				table_element* new_element = (table_element*)malloc(sizeof(table_element));
				new_element->type=parametro->type;
				return new_element;
			}
			parametro=parametro->next;		
		}		
		//procurar na global, caso não encontre na sua funcao
		aux = tabela->previous;
		while(aux!=NULL){
			if(aux->isVarDec==1 && strcmp(aux->name,id)==0){
				aux->used = 1;
				return aux;
			}
			aux = aux->previous;
		}
	}
	return NULL;
}

int check_assign(table* global,table* tabela, is_statement* statement){
	int erros=0;
	table_element* res = check_exists_var(tabela,statement->id);
	if(res==NULL){ //cannot find
		printf("Line %d, column %d: Cannot find symbol %s\n",statement->linhaId,statement->colunaId,statement->id);
		erros+=1;	
	}
	//chamar check expression
	basic_type tipo = check_expression(global,tabela,statement->node,&erros);
	if(res!=NULL && res->type!=tipo){
		printf("Line %d, column %d: Operator = cannot be applied to types %s, %s\n",statement->linhaAssign,statement->colunaAssign,name_types[res->type],name_types[tipo]);
		erros+=1;
	}else if(res==NULL){
		printf("Line %d, column %d: Operator = cannot be applied to types undef, %s\n",statement->linhaAssign,statement->colunaAssign,name_types[tipo]);
		erros+=1;
	}
	return erros;
}

basic_type check_expression(table* global,table* tabela, is_nodeExp* node,int* erros){
	is_nodeExp* aux = node;
	if(aux->operation!=NULL){
		basic_type left=-1,right=-1;
		
		if(strcmp(aux->operation,"Call")==0){
			is_nodeExp* leftNode = aux->left;
			//procurar funcao
			table* auxGlobal = global->nextFunction;
			while(auxGlobal!=NULL){
				if(strcmp(auxGlobal->name,aux->num->numero)==0){
					break;				
				}
				auxGlobal = auxGlobal->nextFunction;			
			}
			if(auxGlobal==NULL){
				*erros = *erros+1;
				//error cannot find symbol
				leftNode = aux->left;
				while(leftNode!=NULL){
					check_expression(global,tabela,leftNode->right,erros);
					leftNode = leftNode->left;
				}
				printf("Line %d, column %d: Cannot find symbol %s(",aux->linha,aux->coluna,aux->num->numero);
				leftNode = aux->left;
				while(leftNode!=NULL){
					if(leftNode->left==NULL)//ultimo
						printf("%s",name_types[leftNode->right->type]);
					else
						printf("%s,",name_types[leftNode->right->type]);
					leftNode=leftNode->left;	
				}
				printf(")\n");
				//printf("Line %d, column %d: Cannot find symbol %s()\n",aux->linha,aux->coluna,aux->num->numero);
				return aux->type=undef;
			}else{
				int state = 0;
				int nParams =0;
				int num=0;
				paramList* parametro = auxGlobal->listaParam;
				while(parametro!=NULL){
					nParams++;
					parametro=parametro->next;				
				}
				parametro = auxGlobal->listaParam;
				while(leftNode!=NULL){ //percorrer os parametros da funcao chamada
					check_expression(global,tabela,leftNode->right,erros);
					if(parametro!=NULL){ 
						if(leftNode->right->type!=parametro->type){
							state = 1;
							*erros = *erros +1;
						}
						parametro = parametro->next;
					}
					leftNode = leftNode->left;
					num++;
				}
				if(num!=nParams){
					state = 1;
					*erros = *erros +1;
				}
				if(state==1){
					printf("Line %d, column %d: Cannot find symbol %s(",aux->linha,aux->coluna,aux->num->numero);
					leftNode = aux->left;
					while(leftNode!=NULL){
						if(leftNode->left==NULL)//ultimo
							printf("%s",name_types[leftNode->right->type]);
						else
							printf("%s,",name_types[leftNode->right->type]);
						leftNode=leftNode->left;	
					}
					printf(")\n");
					return aux->type=undef;
				}
				return aux->type = auxGlobal->returnType;
			}
		}else{
			if(aux->left!=NULL){
				left=check_expression(global,tabela,aux->left,erros);	
			}
			if(aux->right!=NULL){
				right=check_expression(global,tabela,aux->right,erros);	
			}
			return aux->type=check_types(aux,left,right,erros);
		}
	}
	else{
		//estamos numa folha, temos de anotar e retornar o tipo
		//saber se a minha variável existe, se for um id	
		if(strcmp(aux->num->tipo,"Id")==0){
			table_element * res = check_exists_var(tabela,aux->num->numero);
			if(res==NULL){
				printf("Line %d, column %d: Cannot find symbol %s\n",aux->linha,aux->coluna,aux->num->numero);
				*erros = *erros+1;
				return aux->type = undef;
			}
			return 	aux->type = res->type;	
		}
		//verificar octal
		if(strcmp(aux->num->tipo,"IntLit")==0){
			if(check_octal(aux->num->numero)==1){
				printf("Line %d, column %d: Invalid octal constant: %s\n",aux->linha,aux->coluna,aux->num->numero);
				*erros = *erros+1;
				return aux->type=integer;
			}
		}
		return aux->type=getType(aux->num->tipo);
			
	}
}

int check_octal(char *numero){
	int i;
	if(numero[0]=='0' && (numero[1]!='x' && numero[1]!='X')){
		for(i=1;i<strlen(numero);i++){
			int num = numero[i]-'0';
			if(num>=8){
				return 1;			
			}
		}
	}
	return 0;
}

void printTables(table* tab){
	table* aux = tab->nextFunction;
	printf("===== Global Symbol Table =====\n");
	table_element* aux2 = tab->nextDecl;
	while(aux2!=NULL){
		if(aux2->isVarDec){ //Variavel
			printf("%s\t\t%s\n",aux2->name,name_types[aux2->type]);
		}else{  //Funcao
			table *funcao=aux2->funcTable;
			if(funcao->listaParam==NULL){//Nao tem parametros
				printf("%s\t()\t%s\n",funcao->name,name_types[funcao->returnType]);	
			}
			else{ //Printar todos os parametros
				printf("%s\t(",funcao->name);
				paramList* listaParam=funcao->listaParam;
				while(listaParam!=NULL){
					if(listaParam->next==NULL)//ultimo
						printf("%s",name_types[listaParam->type]);
					else
						printf("%s,",name_types[listaParam->type]);
					listaParam=listaParam->next;
				}
				printf(")\t%s\n",name_types[aux2->type]);				
			}
			
		}
		aux2=aux2->next;
	}
	while(aux!=NULL){
		printf("\n");
		printf("===== Function %s(",aux->name);
		//parametros
		paramList* listaParam=aux->listaParam;
		while(listaParam!=NULL){
			if(listaParam->next==NULL)//ultimo
				printf("%s",name_types[listaParam->type]);
			else
				printf("%s,",name_types[listaParam->type]);
			listaParam=listaParam->next;
		}
		printf(") Symbol Table =====\n");
		printf("return\t\t%s\n",name_types[aux->returnType]);
		paramList* listaParams=aux->listaParam;
		while(listaParams!=NULL){
			printf("%s\t\t%s\tparam\n",listaParams->id,name_types[listaParams->type]);
			listaParams=listaParams->next;
		}
		//Body Func
		table_element* varFunc=aux->nextDecl;
		while(varFunc!=NULL){
			if(varFunc->open_close==0)
				printf("%s\t\t%s\n",varFunc->name,name_types[varFunc->type]);
			varFunc=varFunc->next;
		}
		aux = aux->nextFunction;
	}
}


basic_type check_types(is_nodeExp* aux,basic_type left, basic_type right,int* erros){
	if(strcmp("Not",aux->operation)==0){
		if(left!=boolean){
			printf("Line %d, column %d: Operator ! cannot be applied to type %s\n",aux->linha,aux->coluna,name_types[left]);
			*erros = *erros+1;	
		}
		return boolean;		
	}if(strcmp("Minus",aux->operation)==0){
		if(left !=integer && left!=float32){
			printf("Line %d, column %d: Operator - cannot be applied to type %s\n",aux->linha,aux->coluna,name_types[left]);
			*erros = *erros+1;
			return undef;
		}
		return left;			
	}if(strcmp("Plus",aux->operation)==0){
		if(left !=integer && left!=float32){
			printf("Line %d, column %d: Operator + cannot be applied to type %s\n",aux->linha,aux->coluna,name_types[left]);
			*erros = *erros+1;
			return undef;
		}
		return left;
	}if(strcmp("Or",aux->operation)==0){
		if(left!=right || left!=boolean){
			printf("Line %d, column %d: Operator || cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("And",aux->operation)==0){
		if(left!=right || left!=boolean){
			printf("Line %d, column %d: Operator && cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("Lt",aux->operation)==0){
		if(left!=right || left==boolean){
			printf("Line %d, column %d: Operator < cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("Gt",aux->operation)==0){
		if(left!=right || left==boolean){
			printf("Line %d, column %d: Operator > cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("Eq",aux->operation)==0){
		if(left!=right){
			printf("Line %d, column %d: Operator == cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;
		}
		return boolean;
	}
	if(strcmp("Ne",aux->operation)==0){
		if(left!=right){
			printf("Line %d, column %d: Operator != cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("Le",aux->operation)==0){
		if(left!=right || left==boolean){
			printf("Line %d, column %d: Operator <= cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("Ge",aux->operation)==0){
		if(left!=right || left==boolean){
			printf("Line %d, column %d: Operator >= cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;		
		}
		return boolean;
	}
	if(strcmp("Add",aux->operation)==0){ 
		if(left!=right || (left!=integer && left!=float32 && left !=string)){
			printf("Line %d, column %d: Operator + cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;
			return undef;
		}
		return left;
	}
	if(strcmp("Sub",aux->operation)==0){
		if(left!=right || (left!=integer && left!=float32) ){
			printf("Line %d, column %d: Operator - cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;
			return undef;
		}
		return left;
	}
	if(strcmp("Mul",aux->operation)==0){
		if(left!=right || (left!=integer && left!=float32) ){
			printf("Line %d, column %d: Operator * cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;
			return undef;
		}
		return left;
	}
	if(strcmp("Div",aux->operation)==0){
		if(left!=right || (left!=integer && left!=float32) ){
			printf("Line %d, column %d: Operator / cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;
			return undef;
		}
		return left;
	}
	if(strcmp("Mod",aux->operation)==0){
		if(left!=integer || right!=integer){
			printf("Line %d, column %d: Operator %% cannot be applied to types %s, %s\n",aux->linha,aux->coluna,name_types[left],name_types[right]);
			*erros = *erros+1;
			return undef;
		}
		return left;
	}
	return undef;
}
