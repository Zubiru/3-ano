#ifndef STRUCTURES_H
#define STRUCTURES_H
#include "symbol_table.h"
/*DECLARE STRUCTS HERE*/

/*typedef struct _s10{
	char* num1;
	char* num2;
	char* op;
	struct _s10* next;
} is_exp_list;*/

typedef struct _s11{
	char* numero;
	char* tipo;
} is_number;

typedef struct _s10{
	char* operation;
	is_number* num;
	int linha;
	int coluna;
	basic_type type;
	struct _s10* left;
	struct _s10* right;
} is_nodeExp;

typedef struct _s9{
	char* identificador;	
	is_nodeExp* node;
	struct _s12* next;
	struct _s12* elseNext;
	char* id;
	int linhaId;
	int colunaId;
	int linhaAssign;
	int colunaAssign;
} is_statement;

typedef struct _s12{
	struct _s9* statement;
	struct _s12* next;
}is_statelist;

typedef struct _s3{
	char *id;
	char *tipo;
	int linha;
	int coluna;
} is_vardec;

typedef struct _s8{
	is_vardec* valV;
	is_statement* valS;
	struct _s8* next;
}is_func_body;

typedef struct _s7{
	char* id;
	char* tipo;
	int linha;
	int coluna;
	struct _s7* next;
} is_func_params;

typedef struct _s6{
	char* id;
	char* tipo;//opcional
	int linha;
	int coluna;
	is_func_params* parametros;
} is_func_header;

typedef struct _s5{
	is_func_header* header;
	is_func_body* body;
} is_funcdec;

typedef struct _s4{
	char *id;
	int linha;
	int coluna;
	struct _s4* next;
} is_id_list;


typedef struct _s2 {
	is_vardec* valV;
	is_funcdec* valF;
	struct _s2* next;
} is_declaration_list;

typedef struct _s1{
	is_declaration_list* dlist;
} is_program;

#endif
